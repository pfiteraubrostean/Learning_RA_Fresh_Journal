\section{The Determinizer}

The login example of Figure~\ref{fig:login} shows that input deterministic register
automata may exhibit nondeterministic behavior: in each run the automaton may generate different output values (passwords).
This is a useful feature since it allows us to model the actual behavior of real-world systems,
but it is also problematic since learning tools such as LearnLib can only handle deterministic systems.
In this section, we show how this type of nondeterminism can be eliminated by exploiting
symmetries that are present in register automata. 

As a first step, we show that each trace is equivalent to a `neat' trace in which fresh values
are selected according to some fixed rules. The concept of `neat' traces is similar to the encoding with `representative' traces
that was used in \cite{CaHJMS2011} in a setting without fresh values.

\begin{definition}[Fresh and neat]
\label{fresh and neat}
Consider a trace $\beta$ of register automaton $\R$:
\begin{eqnarray}
\label{eq:beta}
\beta & = & i_0 (d_0) ~ o_0(e_0) ~ i_1 (d_1) ~ o_1 (e_1) ~ \cdots ~ i_{n-1} (d_{n-1}) ~ o_{n-1} (e_{n-1})
\end{eqnarray}
Let $S_j$ be the set of values that occur in $\beta$ before input $i_j$,
and let $T_j$ be the set of values that occur before output $o_j$, that is,
$S_0 = \emptyset$, $T_j = S_j \cup \{ d_j \}$ and $S_{j+1} = T_j \cup \{ e_j \}$.
An input value $d_j$ is \emph{fresh} if it has not occurred before in the trace, that is, $d_j \not\in S_j$.
Similarly, an output value $e_j$ is \emph{fresh} if it has not occurred before, that is, $e_j \not\in T_j$.
We say that $\beta$ has \emph{neat inputs} if each fresh input value $d_j$
is equal to the largest preceding value (including $0$) plus one, that is, $d_j \in S_j \cup \{ \max(S_j \cup \{ 0 \}) + 1 \}$.
Similarly, $\beta$ has \emph{neat outputs} if each fresh output value is equal to the smallest preceding value 
(including $0$) minus one, that is,  for all $j$, $e_j \in T_j \cup \{ \min(T_j\cup \{ 0 \}) - 1 \}$.
A trace is \emph{neat} if it has neat inputs and neat outputs, and a run is \emph{neat} if its trace is neat.
\end{definition}

Observe that in a neat trace the $n$-th fresh input value is $n$, and the $n$-th fresh output value is $-n$.

\begin{example}
\label{Ex:neat}
Trace $i(1) ~ o(3) ~ i(7) ~ o(7) ~ i(3) ~ o(2)$ is not neat, for instance because the first fresh output value $3$ is
not equal to $-1$.
Also, the second input value $7$ is fresh but different from $4$, the largest preceding value plus $1$.
An example of a neat trace is $i(1) ~ o(-1) ~ i(2) ~ o(2) ~ i(-1) ~ o(-2)$.
\end{example}

The next proposition implies that in order to learn the behavior of a register automaton it suffices
to study its neat traces, since any other trace is equivalent to a neat trace. In order to prove
this result, we need the following technical definition, which extends any finite one-to-one
relation to an automorphism.

\begin{definition}
For each finite set $S \subseteq \integers$, let $\Compl(S)$ be a function that enumerates the elements in the complement of $S$,
that is, $\Compl(S) : \nat \rightarrow (\integers\setminus S)$ is a bijection.
Then, for any finite one-to-one relation $r \subseteq \integers\times\integers$, $\hat{r}$ is the automorphism given by:
\begin{eqnarray*}
\hat{r} & = & r \cup \{(\Compl(\dom(r))(k),\Compl(\ran(r))(k)) \mid k \in \nat \}.
\end{eqnarray*}
Here $\dom(r)$ denotes the domain of $r$ and $\ran(r)$ denotes the range of $r$.
\end{definition}

\begin{proposition}
\label{lemma neat}
For every trace $\beta$ there exists a zero respecting automorphism $h$ such that $h(\beta)$ is neat.
\end{proposition}
\iflong
\begin{proof}
Let $\beta$, $S_j$ and $T_j$ ($j= 0,\ldots, n-1$) be as in Definition~\ref{fresh and neat}.
Inductively, we define relations $s_j , t_j \subseteq \integers \times \integers$ (for $j= 0,\ldots, n-1$) as follows
\begin{eqnarray*}
s_0 & = & \emptyset\\
t_j & = & \left\{ \begin{array}{ll}
					s_j \cup \{ (d_j, \max(\ran(s_j)\cup \{ 0 \})+1)\}& \mbox{ if } d_j \mbox{ is fresh}\\
					s_j & \mbox{ otherwise}
				  \end{array}\right.\\
s_{j+1} & = & \left\{ \begin{array}{ll}
					t_j \cup \{ (e_j, \min(\ran(t_j)\cup \{ 0 \})-1)\}& \mbox{ if } e_j \mbox{ is fresh}\\
					t_j & \mbox{ otherwise}
				  \end{array}\right.				  
\end{eqnarray*}

By induction, we can prove the following assertions, for all $j$:
(1) $\dom(s_j) = S_j$ and $\dom(t_j) = T_j$,
(2) $s_j$ and $t_j$ are injective.
By construction, $t_{n-1} (\beta)$ is neat.
Then $h ~=~ \hat{t}_{n-1}$ is an automorphism such that $h(\beta)$ is neat.
\qed
\end{proof}
\fi

\begin{example}
Consider the trace $i(1)$  $o(3)$ $i(7)$ $o(7)$ $i(3)$ $o(2)$ from Example~\ref{Ex:neat}.
This non neat trace can be mapped to the neat trace $i(1)$ $o(-1)$ $i(2)$ $o(2)$ $i(-1)$ $o(-2)$ by the automorphism $h$ 
that acts as the identity function except that it permutes some values:
$h(3) = -1$, $h(-1) = 7$, $h(7) = 2$, $h(2) = -2$, and $h(-2) = 3$.
\end{example}

\begin{corollary}
For every run $\alpha$ of $\R$ there exists an automorphism $h$ such that $h(\alpha)$ is neat.
\end{corollary}
\iflong
\begin{proof}
Let $\alpha$ be a run of $\R$.
Then $\beta = \textsf{trace}(\alpha)$ is a trace of $\R$.
Therefore, by Proposition~\ref{lemma neat},
there exists an automorphism $h$ such that $h(\beta)$ is neat.
By Lemma~\ref{automorphism preserves runs}, $h(\alpha)$ is a run of $\R$ and by Lemma~\ref{trace and automorphism commute},
$\textsf{trace}(h(\alpha)) = h (\beta)$.
Since $h(\beta)$ is neat and a run is neat if its trace is neat, $h(\alpha)$ is neat as well.
\qed
\end{proof}
\fi

Whereas the learner may choose to only provide neat inputs, we usually have no control over the outputs generated
by the SUL, so in general these will not be neat.
In order to handle this, we place a component, called the \emph{determinizer}, in between the SUL and the learner.
The determinizer renames the outputs generated by the SUL and makes them neat.
%: the first fresh output value generated by the SUL is renamed to $-1$, the second to $-2$, etc.
The behavior of the determinizer is specified by the mapper $\D$ defined below.
As part of its state $\D$ maintains a finite one-to-one relation $r$ describing the current renamings,
which grows dynamically during an execution (similar to the functions $s_j$ and $t_j$ in the proof of
Proposition~\ref{lemma neat}).
We write $\hat{r}$ for an automorphism that extends $r$ 
(we may construct $\hat{r}$ using the construction described in the proof of Proposition~\ref{lemma neat}).
Whenever the SUL generates an output $n$ that does not occur in $\dom(r)$,
this output is mapped to a value $m$ one less than the minimal value in $\ran(R)$, and the pair $(n, m)$ is added to $r$.
Whenever the learner generates an input $m$, the mapper concretizes this value to $n = \hat{r}^{-1} (m)$ and
forwards $n$ to the SUL. If $n$ does not occur in $\dom(r)$, then $r$ is extended with the pair $(n, m)$.

\begin{definition}[Determinizer]
\label{determinizer}
Let $I$ and $O$ be finite, disjoint sets of input and output symbols.
%NOTE: Slight abuse of notation, I refer to both the mapper and the induced component as determinizer
The \emph{determinizer} for $I$ and $O$ is the mapper 
$\D = \langle (I \times \integers) \cup (O \times \integers), (I \times \integers) \cup (O \times \integers),  R, r_0, \delta, \abstr \rangle$ where
\begin{itemize}
\item
$R = \{ r \subseteq \integers\times\integers \mid r \mbox{ finite and one-to-one} \}$,
\item
$r_0 = \emptyset$,
\item
for all $r \in R$, $i \in I$, $o \in O$ and $n \in\integers$,
\begin{eqnarray*}
\lambda(r, i(n)) & = & i( \hat{r}(n))\\
\lambda(r, o(n)) & = & \left\{ \begin{array}{ll}
                         o(r(n)) &\mbox{ if } n \in \dom(r)\\
                         o(\min(\ran(r) \cup \{ 0 \})-1) &\mbox{ otherwise}
                        \end{array} \right.\\
\delta(r, i(n)) & = & \left\{ \begin{array}{ll}
						r &\mbox{ if } n \in \dom(r)\\
						r \cup \{(n,\hat{r}(n)) \}&\mbox{ otherwise}
						\end{array} \right.\\
\delta(r, o(n)) & = & \left\{ \begin{array}{ll}
                         r &\mbox{ if } n \in \dom(r)\\
                         r \cup \{(n,\min(\ran(r) \cup \{ 0 \})-1) \} &\mbox{ otherwise}
                        \end{array} \right.
\end{eqnarray*}
\end{itemize}
\end{definition}

\begin{proposition}
\label{inclusion1}
Let $\R$ be a register automaton with inputs $I$ and outputs $O$,
let $\D$ be the determinizer for $I$ and $O$, and
let $\beta$ be a trace of $\ABS{\sem{\R}}{\D}$.
Then $\beta$ has neat outputs and is equivalent to a trace of $\R$.
\end{proposition}
\iflong
\begin{proof}
Let $\alpha$ be a run of $\ABS{\sem{\R}}{\D}$ with trace $\beta$.
We claim that $\alpha$ does not contain any transitions with output $\perp$, that is,
transitions generated by the second rule in Definition~\ref{def:abstr}.
This is because, for any state $r$ of mapper $\D$ and any `abstract' input $i(d)$,
there exists a `concrete' input $i(d')$ such that $\lambda(r, i(d')) = i(d)$.
In fact, since $\hat{r}$ is an automorphism, we can just take $d' = \hat{r}^{-1}(d)$.
Hence run $\alpha$ takes the form
\[
\alpha = ((l_0, \xi_0), r_0) ~ i_0 (d_0) ~ o_0(e_0) ~((l_1, \xi_1), r_1) ~ i_1 (d_1) ~ o_1 (e_1) ~ ((l_2, \xi_2), r_2)\cdots
\]
\[
\hspace{4cm} \cdots  ~ i_{n-1} (d_{n-1}) ~ o_{n-1} (e_{n-1}) ~ ((l_n, \xi_n), r_n).
\]
Since the transitions in run $\alpha$ have been derived by repeated application of the first rule in Definition~\ref{def:abstr},
there exist $d'_j$, $e'_j$ and $r'_j$ such that $\sem{\R}$ has a run $\alpha'$ of the form
\[
\alpha' = (l_0, \xi_0) ~ i_0 (d'_0) ~ o_0(e'_0) ~(l_1, \xi_1) ~ i_1 (d'_1) ~ o_1 (e'_1) ~ (l_2, \xi_2) \cdots
\]
\[
\hspace{4cm} \cdots  ~ i_{n-1} (d'_{n-1}) ~ o_{n-1} (e'_{n-1}) ~ (l_n, \xi_n),
\]
and $\D$ has a run
\[
r_0 ~ i_0 (d'_0) ~ i_0(d_0) ~ r'_0 ~ o_0(e'_0) ~ o_0(e_0) ~ r_1 ~ i_1 (d'_1) ~ i_1(d_1) ~ r'_1 ~ o_1 (e'_1) ~ o_1 (e_1) ~  r_2 \cdots
\]
\[
\hspace{2.5cm} \cdots  ~ i_{n-1} (d'_{n-1}) ~ i_{n-1} (d_{n-1}) ~ r'_{n-1} ~ o_{n-1} (e'_{n-1}) ~ o_{n-1} (e_{n-1}) ~  r_n .
\]
From Definition~\ref{determinizer} we may infer that, for all $j<n$, $(d'_j,d_j) \in r'_j$,
$(e'_j, e_j) \in r_{j+1}$, $r_j \subseteq r'_j$ and $r'_j \subseteq r_{j+1}$.
Now let $h = \hat{r}_n$. Then $h$ is an automorphism satisfying, for all $j<n$, 
$h(d'_j)=d_j$ and $h(e'_j)=e_j$.
Let $\beta'$ be the trace of $\alpha'$.
Then $h(\beta') = \beta$ and thus traces $\beta$ and $\beta'$ are equivalent.

Let $S_j$ be the set of values that occur in $\beta$ before input $i_j$,
and let $T_j$ be the set of values that occur in $\beta$ before output $o_j$.
Then it follows by induction that $S_j = \ran(r_j)$ and $T_j = \ran(r'_j)$.
According to Definition~\ref{fresh and neat}, $\beta$ has neat outputs if $e_j \in T_j \cup \{ \min(T_j \cup\{ 0 \}) - 1 \}$, 
that is,
if $e_j \in \ran(r'_j) \cup \{ \min(\ran(r'_j \cup \{ 0 \}) ) - 1 \}$. But this is implied by Definition~\ref{determinizer}.
\qed
\end{proof}
\fi

\begin{proposition}
\label{inclusion2}
Any trace of $\R$ with neat outputs is also a trace of $\ABS{\sem{\R}}{\D}$.
\end{proposition}
\iflong
\begin{proof}
Let $\alpha$ be a run of $\sem{\R}$ with trace $\beta$. Then run $\alpha$ takes the form
\[
\alpha = (l_0, \xi_0) ~ i_0 (d_0) ~ o_0(e_0) ~(l_1, \xi_1) ~ i_1 (d_1) ~ o_1 (e_1) ~ (l_2, \xi_2) \cdots
\]
\[
\hspace{4cm} \cdots  ~ i_{n-1} (d_{n-1}) ~ o_{n-1} (e_{n-1}) ~ (l_n, \xi_n).
\]
$\ABS{\sem{\R}}{\D}$ has a corresponding run $\alpha'$ of the form
\[
\alpha' = ((l_0, \xi_0), r_0) ~ i_0 (d'_0) ~ o_0(e'_0) ~((l_1, \xi_1), r_1) ~ i_1 (d'_1) ~ o_1 (e'_1) ~ ((l_2, \xi_2), r_2)\cdots
\]
\[
\hspace{4cm} \cdots  ~ i_{n-1} (d'_{n-1}) ~ o_{n-1} (e'_{n-1}) ~ ((l_n, \xi_n), r_n)
\]
and $\D$ has a run
\[
r_0 ~ i_0 (d_0) ~ i_0(d'_0) ~ r'_0 ~ o_0(e_0) ~ o_0(e'_0) ~ r_1 ~ i_1 (d_1) ~ i_1(d'_1) ~ r'_1 ~ o_1 (e_1) ~ o_1 (e'_1) ~  r_2 \cdots
\]
\[
\hspace{2.5cm} \cdots  ~ i_{n-1} (d_{n-1}) ~ i_{n-1} (d'_{n-1}) ~ r'_{n-1} ~ o_{n-1} (e_{n-1}) ~ o_{n-1} (e'_{n-1}) ~  r_n .
\]
Let $S_j$ be the set of values that occur in $\beta$ before input $i_j$,
and let $T_j$ be the set of values that occur in $\beta$ before output $o_j$.
Then it follows by induction that $S_j = \dom(r_j)$ and $T_j = \dom(r'_j)$.
Since $\beta$ has neat outputs, $e_j \in \dom(r'_j) \cup \{ \min(\dom(r'_j)\cup \{ 0 \}) - 1 \}$.
Let $\mathit{Id}$ denote the identity function on $\integers$, that is, $\mathit{Id} = \{ (n,n) \mid n \in\integers \}$.
Observe that for any finite one-to-one relation $r \subseteq \mathit{Id}$, $\hat{r} = \mathit{Id}$.
By induction on $j$, we may now prove that $r_j, r'_j \subseteq \mathit{Id}$.
It follows that $d_j = d'_j$ and $e_j = e'_j$, for all $j$.
Thus $\beta$ is a trace of $\ABS{\sem{\R}}{\D}$, as required.
\qed
\end{proof}
\fi

\begin{corollary}
$\R$ and $\ABS{\sem{\R}}{\D}$ have equivalent traces.
\end{corollary}
\iflong
\begin{proof}
Immediate from Propositions~\ref{lemma neat}, \ref{inclusion1} and \ref{inclusion2}.
\qed
\end{proof}
\fi

\begin{example}
\label{Ex:collision}
The determinizer does not remove all sources of nondeterminism.
The login model of Figure~\ref{fig:login}, for instance, is not behavior deterministic, even when we only consider neat traces,
because of neat traces $\register(1) ~ \OK(1)$ and $\register(1) ~ \OK(-1)$.
This nondeterminism may be considered `harmless' since the parameter value of the $\OK$-output 
is not stored and the behavior after the different outputs is the same.
The slot machine model of Figure~\ref{fig:nondeterminism}, however, has real nondeterminism
in the sense that traces $\BUTTON(1) ~ \REEL(-1) ~ \BUTTON(1) ~ \REEL (-2)$ and
$\BUTTON(1) ~ \REEL(-1) ~ \BUTTON(1) ~ \REEL (-1)$ lead to states with distinct output symbols in the
outgoing transitions.
\end{example}

The slot machine of Example~\ref{Ex:collision} nondeterministically selects an output which 
`accidentally' may be equal to a previous value. We call this a \emph{collision}.

\begin{definition}
Let $\beta$ be a trace of register automaton $\R$. 
Then $\beta$ \emph{ends with a collision} if 
(a) the last output value is not fresh, and 
(b) the sequence obtained by replacing this value by some other value is also a trace of $\R$.
We say that $\beta$ \emph{has a collision} if it has a prefix that ends with a collision.
\end{definition}

\begin{example}
Trace \BUTTON(3) ~ \REEL(137) ~ \BUTTON(8) ~ \REEL(137) of the slot machine model of Figure~\ref{fig:nondeterminism} has a collision,
because the last output value $137$ is not fresh, and if we replace it by $138$ the result is again a trace.
\end{example}

In many protocols, fresh output values are selected from a finite but large domain.
TCP sequence and acknowledgement numbers, for instance, comprise 32 bits.
The traces generated during learning are usually not so long and typically contain only a few fresh outputs. 
As a result, chances that collisions occur during learning are typically negligible.
%In fact, during all our experiments to learn models of the TCP protocol \cite{FJV14} we have never observed a collision.
For these reasons, we have decided to consider only observations without collisions.
Under the assumption that the SUL will not repeatedly pick the same fresh value,
we can detect whether an observation contains a collision by simply repeating experiments a few times:
if, after the renaming performed by the determinizer, we still observe nondeterminism then a collision has occurred.
By ignoring traces with collisions, it may occur that the models that we learn incorrectly 
describe the behavior of the SUL in the case of collisions. 
We will, for instance, miss the \WIN-transition in the slot machine of Figure~\ref{fig:nondeterminism}.
But if collisions are rare then it is extremely difficult to learn those types of behavior anyway.
In applications with many collisions (for instance when fresh outputs are selected randomly from a small domain) one should
not use the approach in this article, but rather an algorithm for learning nondeterministic automata
such as the one presented in \cite{VolpatoT15}.

Our approach for learning register automata with fresh outputs relies on the following proposition.

\begin{proposition}
The set $S$ of collision free neat traces of an input deterministic register automaton $\R$ is behavior deterministic.
\end{proposition}
\iflong
\begin{proof}
Let $\R= \langle I, O, L, l_{0}, V, \Gamma\rangle$ be an input deterministic register automaton and 
let $S$ be the set of collision free neat traces of $\R$.
Suppose that $\beta \; i(d) \; o(e)$ and $\beta \; i(d) \; o'(e')$ are traces in $S$. 
Our task is to prove that $o(e) = o'(e')$.
Since $\R$ is input deterministic, there is a unique run $\alpha$ of $\sem{\R}$ with trace $\beta$.
Let $(l, \xi)$ be the last state of this run.
Since $\beta \; i(d) \; o(e)$ and $\beta \; i(d) \; o'(e')$ are traces of $\R$,
$\sem{R}$ has transitions $(l,\xi)\xrightarrow{i(d) / o(e)}(l_1,\xi_1)$ and
$(l,\xi)\xrightarrow{i(d) / o'(e')}(l'_1,\xi'_1)$.
Since $\R$ is input deterministic, there is a unique transition 
that supports both transitions of $\sem{\R}$ and thus $o = o'$.
We consider two cases.
If both values $e$ and $e'$ are fresh then, since traces $\beta \; i(d) \; o(e)$ and $\beta \; i(d) \; o'(e')$ are neat,
$e$ and $e'$ are both equal to the smallest preceding value minus one and thus $e=e'$.
Now assume that at least one value, say $e$, is not fresh.
Then, since $\beta \; i(d) \; o(e)$ is collision free, no sequence obtained from $\beta \; i(d) \; o(e)$ by
replacing $e$ by some other value can be a trace of $\R$. Thus $e = e'$ also in this case.
We conclude  $o(e) = o'(e')$, as required.
\qed
\end{proof}
\fi

Our learning approach works for those register automata in which,
when a fresh output is generated, it does not matter for the future behavior
whether or not this fresh output equals some value that occurred previously.
This is typically the case for real-world systems such as servers that generate
fresh identifiers, passwords or sequence numbers.
The slot machine of Figure~\ref{fig:nondeterminism} and Figure~\ref{fig:nondeterminism2}
is an example of a system that we cannot learn.

\begin{proposition}
Let $\R_1$ and $\R_2$ be two input deterministic right invariant register automata in which $\Out$ does
not occur negatively in guards.
Then $\R_1$ and $\R_2$ are equivalent iff they have the same sets of collision free traces.
\end{proposition}

%TODO : A theorem showing which class of RIRAs we can actually learn:
%TODO : If two RIRAs in which out does not occur negatively in guards agree on collision free traces then
%TODO : they agree on all traces
