\section{Model Learning}
Algorithms for active learning of automata have originally been developed for
inferring finite state acceptors for unknown regular
languages~\cite{Ang87}. Since then these algorithms have become
popular with the testing and verification communities for inferring
models of black box systems in an automated fashion.
% Active automata learning
%has been extended to many classes of systems, including Mealy Machines \cite{Nie03},
% I/O-Automata \cite{AV10}, Timed Automata \cite{GrinchteinJP06}, and Register Automata.
%
While the details change for concrete classes of systems, all of these
algorithms follow basically the same pattern. They
model the learning process as a game between a learner and a teacher. The learner has
to infer an unknown automaton with the help of the teacher. The learner can ask three types of queries to the teacher:
%The teacher answers two kinds of queries.
%
\begin{description}
\item[Output Queries] ask for the expected output for a concrete sequence of inputs. In practice, output queries can be realized as simple tests.
%consisting of a sequence of input symbols followed by a reset.

\item[Reset Queries] prompt the teacher to return to its initial state and are typically asked after each output query.

\item[Equivalence Queries] check whether a conjectured automaton produced
by the learner is correct. In case the automaton is not correct, the teacher
provides a counterexample, a trace exposing a difference between
the conjecture and the expected behavior of the system to be learned.
Equivalence queries can be approximated through (model-based) testing in black-box scenarios.
\end{description}
%
A learning algorithm will use these three kinds of queries and produce
a sequence of automata converging towards the correct one in a finite number of steps.
We refer the reader to~\cite{SteffenHM11,IsbernerHS14,Vaa17} for introductions to active automata learning.

\subsection{The Nerode congruence}
Most of the learning algorithms that have been proposed in the literature aim to construct an approximation
of the Nerode congruence  based on a finite number of queries.
The famous Myhill-Nerode theorem \cite{nerode58} for Deterministic Finite Automata (DFA)
provides a basis for describing (a) how prefixes traverse states
(equivalence classes), and (b) how states can be distinguished (by suffixes).
Below we present a straightforward reformulation of the Myhill-Nerode theorem for deterministic Mealy machines,
adapted from \cite{SteffenHM11}.

\begin{definition}
An \emph{observation} over a set of inputs $I$ and a set of outputs $O$ is a finite alternating sequence
$i_0 o_0 \cdots i_{n-1} o_{n-1}$ of inputs and outputs that is either empty, or begins with an input and
ends with an output. Let $S$ be a set of observations over $I$ and $O$. Then $S$ is
\begin{itemize}
\item
\emph{prefix closed} if
$\beta ~ i ~ o \in S  \implies \beta \in S$,
\item
\emph{behavior deterministic} if
$\beta ~ i ~ o \in S \wedge \beta ~ i ~ o' \in S \implies o = o'$, and
\item
\emph{input complete} if
$\beta\in S \wedge  i  \in I \implies \exists o \in O : \beta ~ i ~ o \in S$.
\end{itemize}
Two observations $\beta, \beta' \in S$ are \emph{equivalent} for $S$, notation $\beta \equiv_S \beta'$, iff for
all observations $\gamma$ over $I$ and $O$, $\beta\gamma \in S \Leftrightarrow \beta'\gamma \in S$.
We write $[\beta]$ to denote the equivalence class of $\beta$ with respect to $\equiv_S$.
\end{definition}

\begin{theorem}[Myhill-Nerode]
\label{theorem myhill nerode}
Let $S$ be a set of observations over finite sets of inputs $I$ and outputs $O$.
Then $S$ is the set of traces of some finite, deterministic Mealy machine $\M$ iff 
$S$ is nonempty, prefix closed, behavior deterministic, input complete, and $\equiv_S$ has finitely many
equivalence classes (finite index).
\end{theorem}
\iflong
\begin{proof}
``$\Rightarrow$''. Let $\M$ be a finite, deterministic Mealy machine and let $S$ be its set of traces.
Then it is immediate from the definitions that $S$ is a nonempty set of observations that is prefix closed and
input complete. Since each trace of $\M$ leads to a unique state and $\M$ is deterministic, it follows that
$S$ is behavior deterministic.
Since all observations that lead to the same state are obviously equivalent and since $\M$ is finite,
equivalence relation $\equiv_S$ has finite index.

``$\Leftarrow$''. Suppose $S$ is nonempty, prefix closed, behavior deterministic, input complete, and $\equiv_S$ has finite index.
We define the finite, deterministic Mealy machine $\M = \langle I, O, Q, q^0, \delta, \lambda \rangle$ as follows:
\begin{itemize}
\item
$Q$ is the set of classes of $\equiv_S$.
\item
$q^0$ is given by $[ \epsilon]$.
\item
Let $\beta \in S$ and $i \in I$. Then, since $S$ is both input complete and behavior deterministic,
there exists a unique $o \in O$ such that $\beta ~ i ~ o \in S$.
We define $\delta([\beta], i) = [ \beta ~ i ~ o]$ and $\lambda([\beta], i) = o$.
\end{itemize}
It is straightforward to verify that $\M$ is a well-defined finite, deterministic Mealy machine whose set of traces equals $S$.
\qed
\end{proof}
\fi

The equivalence relation $\equiv_S$ induced by the set of traces $S$ of a register automaton does not have a finite index.
However, as observed by \cite{CaHJMS2011,CasselHJMS15,BKL14}, by using the inherent symmetry of register automata we may define a slightly
different equivalence relation $\equiv_S^{\mathit{aut}}$ that does have a finite index and that may serve as a basis for a Myhill-Nerode theorem for register automata.
The equivalence relation $\equiv_S^{\mathit{aut}}$ on $S$ is defined by
\begin{eqnarray*}
\beta \equiv_S^{\mathit{aut}} \beta' & \Leftrightarrow & \exists \mbox{ automorphism } h ~ \forall \gamma : (\beta \gamma \in S
\Leftrightarrow \beta' h(\gamma) \in S)
\end{eqnarray*}

\begin{proposition}
\label{finite index Nerode equivalence for RAs}
Let $\R$ be an input deterministic register automataton and let $S$ be its set of traces.
Then $\equiv_S^{\mathit{aut}}$ has a finite index.
\end{proposition}
\iflong
\begin{proof}
Since $\R$ is input deterministic, there exists for each trace of $\R$ a unique corresponding run.
Let $\beta$ and $\beta'$ be traces of $\R$ and let $(l,\xi)$ and $(l', \xi')$ be the final states of
the corresponding runs.
Assume that $l=l'$ and $\Part(\xi) = \Part(\xi')$.
(Here we write $\Part(f)$ for the partition induced by function $f$ in which two elements from the domain of $f$
are placed in the same block iff $f$ maps them to the same value.)
Then there exists an automorphism $h$ from $\xi$ to $\xi'$.
By Lemma~\ref{automorphism preserves runs},
$\alpha$ is a partial run starting in $(l,\xi)$ iff $h(\alpha)$ is a partial run starting in $(l', \xi')$.
Moreover, by Lemma~\ref{trace and automorphism commute}, $\trace(h(\alpha)) = h (\trace(\alpha))$.
Hence, $\beta \equiv_S^{\mathit{aut}} \beta'$.
Since $\R$ has a finite number of locations, since each location has a finite set of registers, and since there are
only finitely many partitions of a finite set, this implies that $\equiv_S^{\mathit{aut}}$ has a finite index.
\qed
\end{proof}
\fi

Whereas \cite{VMCAI2012,HISBJ12} presents a learning algorithm for register automata that is based on a variant of the Myhill-Nerode
theorem for $\equiv_S^{\mathit{aut}}$ (i.e., the ``converse'' of Proposition~\ref{finite index Nerode equivalence for RAs}), 
the idea of our approach is to learn register automata by constructing an abstraction
of the set of traces that has a finite index according to the original definition of $\equiv_S$.

