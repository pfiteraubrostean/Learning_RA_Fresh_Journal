\section{Restricted Types of Register Automata}
Cassel et al \cite{CasselHJMS15} introduce the concept of a \emph{right invariant} register automaton
and provide a canonical automaton presentation of any language recognizable by a deterministic right invariant
register automaton. The notion of right invariance plays an important role in our work as well.
In this section, we discuss the formal definition of right invariance and prove some key results.

\begin{definition}
\label{definition right invariance}
Let $\R = \langle I, O, L, l_{0}, V, \Gamma\rangle$ be a register automaton. Then $\R$ is \emph{right invariant} if,
for each transition $l \xrightarrow{i, g, \varrho, o} l'$ in $\Gamma$, $g$ is satisfiable and
\begin{enumerate}
\item
for distinct $x, y \in V(l)$, neither $g \Rightarrow x=y$ nor $g \Rightarrow x \neq y$ is valid, and
\item
the combined effect of guard $g$ and assignment $\varrho$ does not imply $x = y$ for distinct $x, y \in V(l')$
(note that inequalities may be implied).\footnote{We can formalize this second condition by introducing a primed version $x'$ of each variable $x$: for all pairs of distinct variables $x, y \in V(l')$ we require that the implication
$g \wedge ( \bigwedge_{z \in V(l')} z' = \rho(z)) \Rightarrow x'=y'$ is not valid.}
\end{enumerate}
\end{definition}
%
Right invariance says that in guards we may compare input and output values with registers,
but we are not allowed to test for (in)equality of distinct registers. 
Also, assignments may not copy the value of a single register in the source state to two distinct registers in the target state.

\begin{example}
The FIFO-set model of Figure~\ref{fig:fifoSet} and the login model of Figure~\ref{fig:login}
are right invariant.
Figure~\ref{fig:nondeterminism} shows an example of a register automaton that is not right invariant.
\begin{figure}[htb]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.9cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l_1$};
  \node[state] (3) [right of=2] {$l_2$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=2cm] node {\BUTTON/\REEL(\osymbol) \\ $v$:=$\osymbol$} (2)
    (2) edge [text width=2cm] node {\BUTTON/\REEL(\osymbol) \\ $w$:=$\osymbol$} (3)
    (3) edge [loop below, text width=1.5cm] node {$v \neq w$\\ \BUTTON/\LOSE} (2)
        edge [loop above, text width=1.5cm] node {$v = w$\\ \BUTTON/\WIN} (3);
\end{tikzpicture}
\caption{A simple slot machine modeled as a register automaton}
\label{fig:nondeterminism}
\end{figure}
This automaton models a simple slot machine.
By pressing a button a user may stop a spinning reel to reveal a value.
If two consecutive values are equal then the user wins, otherwise he loses.
The automaton is not right invariant, since in location $l_2$ we test for equality the registers $v$ and $w$.
\end{example}

The next lemma provides an equivalent characterization of right invariance.

\begin{lemma}
\label{lemma right invariance}
$\R$ is right invariant iff for all transitions $l \xrightarrow{i, g, \varrho, o} l'$,
$\varrho$ is injective and
guard $g$ is equivalent to a formula of the form $g_{\In} \wedge g_{\Out}$ such that
\begin{enumerate}
\item
$g_{\In} \equiv \In = x$, with $x \in V(l)$, or
$g_{\In} \equiv \bigwedge_{x \in W} \In \neq x$, with $W \subseteq V(l)$
(by convention the conjunction over the empty index set is $\mathsf{true}$),
\item
$g_{\Out} \equiv \Out = x$, with $x \in V(l) \cup \{ \In \}$, or
$g_{\Out} \equiv \bigwedge_{x \in W} \Out \neq x$, with $W \subseteq V(l) \cup \{ \In \}$,
\item
if $g_{\In} \equiv \In = x$, with $x \in V(l)$, then there are no $y, z \in V(l')$ with $\varrho(y)=\In$ and $\varrho(z)=x$,
\item
if $g_{\Out} \equiv \Out = x$, with $x \in V(l) \cup \{ \In \}$, then there are no $y, z \in V(l')$ with $\varrho(y)=\Out$ and $\varrho(z)=x$, and
\item
there is no $x \in V(l)$ with $g_{\In} \equiv \In = x$ and $g_{\Out} \equiv \Out = x$.
\end{enumerate}
\end{lemma}
\iflong
\begin{proof}
``$\Rightarrow$'' Suppose $l \xrightarrow{i, g, \varrho, o} l'$ is a transition of $\R$.
Assume that $x, y \in V(l')$ are distinct variables and $\varrho(x)=\varrho(y)$.
Then $g \wedge ( \bigwedge_{z \in V(l')} z' = \rho(z)) \Rightarrow x'=y'$ is valid, which is a contradiction.
Thus $\varrho$ is injective. 
Since $g$ is satisfiable, it can be written as a conjunction of atomic formula $x = y$ or $x \neq y$ with $x, y$ distinct
variables from $V(l) \cup \{ \In, \Out \}$. 
W.l.o.g.\ we assume that each atomic formula occurs at most once in $g$.
Observe that $g$ does not contain an atomic formula $x = y$ with $x, y$ distinct variables from $V(l)$, because then 
$g \Rightarrow x=y$ would be valid.
Similarly, $g$ does not contain an atomic formula $x \neq y$ with $x, y$ distinct variables from $V(l)$, because then 
$g \Rightarrow x \neq y$ would be valid.
Thus each atomic formula in $g$ either contains variable $\In$ or variable $\Out$ (or both).
W.l.o.g.\ we assume that if an atomic formulas contain $\Out$, variable $\Out$ occurs on the left, 
and otherwise variable $\In$ occurs on the left.
Moreover, we assume w.l.o.g.\ that $g$ does not contain atomic formulas $\In =x$ and $\Out=x$, for some $x \in V(l)$ (in such a case
we may replace $\Out=x$ by $\Out = \In$).
Let $g_{\Out}$ be the conjunction of all atomic formulas from $g$ that contain $\Out$,
and let $g_{\In}$ be the conjunction of all remaining atomic formulas from $g$. Then $g = g_{\In} \wedge g_{\Out}$.
Observe that $g_{\In}$ does not contain subformulas $\In = x$ and $\In = y$, for distinct $x, y \in V(l)$, because then 
$g \Rightarrow x=y$ would be valid.
Similarly, $g_{\In}$ does not contain subformulas $\In = x$ and $\In \neq y$, for distinct $x, y \in V(l)$, because then 
$g \Rightarrow x \neq y$ would be valid. 
Finally, observe that $g_{\In}$ does not contain subformulas $\In = x$ and $\In \neq x$, for $x \in V(l)$, because this 
would contradict satisfiability of $g$.
Condition (1) from the lemma now follows.
Using a similar argument, we may prove condition (2).
Condition (3) follows by contradiction. Suppose that $g_{\In} \equiv \In = x$, $\varrho(y)=\In$ and $\varrho(z)=x$,
for $y, z \in V(l')$.
Then  $g \wedge ( \bigwedge_{u \in V(l')} u' = \rho(u)) \Rightarrow y'=z'$ is valid, which is a contradiction.
Conditions (4) follows via a similar argument.
Condition (5) follows from our assumption that $g$ does not contain atomic formulas $\In =x$ and $\Out=x$, for $x \in V(l)$.

``$\Leftarrow$'' Suppose $l \xrightarrow{i, g, \varrho, o} l'$ is a transition of $\R$.
Let $\zeta : \V \to \integers$ be an injective function. 
Valuation $\xi$ for $V(l) \cup \{ \In, \Out \}$ assigns distinct values to all variables, except when equality is required by $g$:
\begin{eqnarray*}
\xi(z) & = & \left\{ \begin{array}{ll}
\zeta(x) & \mbox{if } z \equiv \Out \wedge g_{\Out} \equiv \Out = x\\
\zeta(\In) & \mbox{if }  g_{\In} \equiv \In = z\\
\zeta(z) & \mbox{otherwise}
\end{array}
\right.
\end{eqnarray*}
It is routine to check that $\xi \models g$, which means that $g$ is satisfiable.
Now suppose that $x$ and $y$ are distinct variables in $V(l)$.
Then $\xi \not\models g \Rightarrow x=y$, which implies that $g \Rightarrow x=y$ is not valid.
Let $n \in \integers$ be a value with $n \neq \zeta(\In)$ and $n \neq \zeta(\Out)$.
Valuation $\overline{\xi}$ for $V(l) \cup \{ \In, \Out \}$ assigns the same value $n$ to all variables, except when inequality is required by $g$:
\begin{eqnarray*}
\overline{\xi}(z) & = & \left\{ \begin{array}{ll}
\zeta(\In) & \mbox{if } z \equiv \In \wedge g_{\In}  \mbox{ contains an inequality}\\
\zeta(\Out) & \mbox{if } z \equiv \Out \wedge g_{\Out}  \mbox{ contains an inequality}\\
n & \mbox{otherwise}
\end{array}
\right.
\end{eqnarray*}
It is again routine to check that $\overline{\xi} \models g$.
Suppose that $x$ and $y$ are distinct variables in $V(l)$.
Then $\overline{\xi} \not\models g \Rightarrow x \neq y$, which implies that $g \Rightarrow x=y$ is not valid.

In order to prove condition (2), suppose $x, y$ are distinct variables from $V(l')$.
Let, for $z \in V(l')$, $\xi' (z') = \xi ( \varrho(z))$.
Then $\xi \cup \xi' \models g \wedge ( \bigwedge_{z \in V(l')} z' = \rho(z))$.
By construction $\xi$ is injective, except that $\xi(\In)=\xi(z)$ in case $g_{\In} \equiv \In = z$,
and $\xi(\Out)= \xi(z)$ in case $g_{\Out} \equiv \Out = z$.
This observation, in combination with the fact that $\varrho$ is injective and conditions (3) and (4) from the statement of the lemma,
gives us that $\xi'$ is injective.
Therefore $\xi \cup \xi'$ does not satisfy $g \wedge ( \bigwedge_{z \in V(l')} z' = \rho(z)) \Rightarrow x'=y'$, which implies that
this formula is not valid.
\qed
\end{proof}
\fi

Lemma~\ref{lemma right invariance} implies that each outgoing transition from a location $l$ of $\R$ may
fire in each state $(l, \xi)$ of $\sem{\R}$, provided we choose the right input and output values.
This has as an important consequence that a location $l$ is reachable in $\R$ iff a state $(l,\xi)$ is
reachable in $\sem{\R}$, for some $\xi$.

\begin{corollary}
\label{Cy right invariance}
Let $\R$ be a right invariant RA with a transition $l \xrightarrow{i, g, \varrho, o} l'$.
Let $\xi \in \Val(V(l))$.
Then $\sem{\R}$ has a transition $(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')$ that is supported by 
$l \xrightarrow{i, g, \varrho, o} l'$.
\end{corollary}
\iflong
\begin{proof}
We may assume that $g$ is of the form $g_{\In} \wedge g_{\Out}$ described in Lemma~\ref{lemma right invariance}.
If $g_{\In}$ of the form $\In = x$, for some $x \in V(l)$, then choose $d = \xi(x)$.
Otherwise, let $d$ be equal to some arbitrary fresh value outside the range of $\xi$.
Similarly, pick a value for $e$.
Then with $ \iota = \xi \cup \{ (\In, d), (\Out, e) \}$ we have $\iota\models g$ by construction, and thus
$(l,\xi)$ enables a transition that is supported by $l \xrightarrow{i, g, \varrho, o} l'$.
\qed
\end{proof}
\fi

Another restriction on register automata that plays an important role in our work is
\emph{unique-valuedness}. Intuitively, this means that registers are required to always store unique values.

\begin{definition}
\label{definition unique valied}
Let $\R = \langle I, O, L, l_{0}, V, \Gamma\rangle$ be a register automaton. Then $\R$ is \emph{unique-valued} if,
for each reachable state $(l, \xi)$ of $\sem{\R}$, valuation $\xi$ is injective, that is, two registers can never
store identical values.
\end{definition}

\begin{example}
The FIFO-set model of Figure~\ref{fig:fifoSet} and the login model of Figure~\ref{fig:login}
are both unique-valued.
The slot machine model of Figure~\ref{fig:nondeterminism} is not unique-valued, since in location $l_2$ registers $v$ and $w$ may
contain the same value.
Figure~\ref{fig:fifo} presents a variation of the FIFO-set model that is right invariant but not unique-valued.
This register automaton, which represents a FIFO-buffer of capacity 2, is not unique-valued since in location $l_2$ registers
$v$ and $w$ may contain the same value.
\begin{figure}[htb]
\centering
\InputTikz{fifo}
\caption{FIFO-buffer with a capacity of 2 modeled as a register automaton}
\label{fig:fifo}
\end{figure}
\end{example}

Even though right invariance and unique-valuedness are strong restrictions it is possible to construct, for each
register automaton, an equivalent register automaton that is both right invariant and unique valued.
Figure~\ref{fig:nondeterminism2}, for example, shows a right invariant and unique-valued register automaton
that is equivalent to the register automaton of Figure~\ref{fig:nondeterminism}.
\begin{figure}[htb]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.9cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l_1$};
  \node[state] (3) [right of=2] {$l_2$};
  \node[state] (4) [below of=2] {$l_3$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=2cm] node {\BUTTON/\REEL(\osymbol) \\ $v$:=$\osymbol$} (2)
    (2) edge [text width=2cm] node {$\osymbol = v$\\  \BUTTON/\REEL(\osymbol)} (3)
    (2) edge [text width=2cm] node {$\osymbol \neq v$\\  \BUTTON/\REEL(\osymbol)} (4)
    (4) edge [loop right, text width=1.5cm] node {\BUTTON/\LOSE} (4)
    (3) edge [loop right, text width=1.5cm] node {\BUTTON/\WIN} (3);
\end{tikzpicture}
\caption{Slot machine modeled as a right invariant register automaton}
\label{fig:nondeterminism2}
\end{figure}

\iflong
\ifverylong
In order to prove that such a construction is always possible, 
we need to introduce several technical definitions and lemmas concerning
partitions, characteristic formulas for partitions, and bisimulations.

\begin{definition}[Partitions]
\label{def partitions}
A \emph{partition} $P$ of a set $S$ is a set of pairwise disjoint non-empty subsets of $S$ whose union is exactly $S$.
Elements of $P$ are called \emph{blocks}. 
If $s \in S$ then $[s]_P$ denotes the unique block of $P$ that contains $s$.
We write $\Pi(S)$ for the set of partitions of $S$.
If $P \in \Pi(S)$ and $T \subseteq S$ then $P \lceil T \in \Pi(S \setminus T)$ denotes the partition
obtained from $P$ by removing elements from $T$, that is,
$P \lceil T = \{ B \setminus T \mid B \in P \mbox{ and } B \setminus T \neq \emptyset \}$.
If $f : S \rightarrow S'$ and $P \in \Pi(S')$ then $f^{-1}(P) \in \Pi(S)$ denotes the inverse image of $P$ under
$f$, that is, $f^{-1}(P) = \{ f^{-1}(B) \mid B \in P \mbox{ and } f^{-1}(B) \neq \emptyset \}$.
Each set $S$ induces a trivial partition $\Part(S) = \{ \{ s \} \mid s \in S \}$, and each
function $f : S \rightarrow S'$ induces a partition $\Part(f) \in \Pi(S)$ in which two elements of $S$ are equivalent iff
$f$ assigns the same value to them: $\Part(f) = f^{-1}(\Part(S'))$.
\end{definition}


Below we consider partitions of finite sets of variables $W$ that are induced by valuations.
Since in formulas we can only talk about equality or inequality of variables,
valuations that induce the same partition satisfy the same formulas.
\begin{lemma}
\label{partitions logic 3}
Suppose $\xi, \xi' \in \Val(W)$ with $\Part(\xi) = \Part(\xi')$ and let $g \in \G(W)$.
Then $\xi \models g ~ \Leftrightarrow ~ \xi' \models g$.
\end{lemma}
\begin{proof}
By induction on the structure of $g$.
\qed
\end{proof}
We assume some well founded ordering on the universe of variables $\V$, providing us with a canonical representative $\min(W)$
in each nonempty set of variables $W \subseteq \V$.
For convenience, we assume $\Out$ is the smallest element of $\V$, and $\In$ is the one but smallest element.
Using representatives, we can describe partitions compactly using formulas.
As an example, consider the following partition:
\begin{eqnarray*}
P & = & \{ \{ x_1 \}, \{ x_2, x_4 \}, \{ x_3, x_5, x_6 \} \}
\end{eqnarray*}
Formula $\phi_{x, P}$ describes the relations of variable $x$ to the rest of $P$. If $x$ is contained in
a singleton block then $\phi_{x, P}$ says that $x$ is different from the representatives of all the other blocks:
\begin{eqnarray*}
\phi_{x_1, P} & \equiv & x_1 \neq x_2 \wedge x_1 \neq x_3 .
\end{eqnarray*}
Otherwise $\phi_{x, P}$ asserts that $x$ is equal to the representative of the remaining variables in the block:
\begin{eqnarray*}
\phi_{x_2, P} & \equiv & x_2 = x_4 .
\end{eqnarray*}
Now partition $P$ can be described by a formula that is constructed inductively: we pick the minimal variable $x$
and conjoin $\phi_{x, P}$ with the formula for the partition obtained by removing $x$ from $P$:
\begin{eqnarray*}
\phi_P & \equiv & (x_1 \neq x_2 \wedge x_1 \neq x_3) \wedge (x_2 = x_4 ) \wedge (x_3 = x_5) \wedge (x_4 \neq x_5) \wedge (x_5 = x_6) \wedge\\
&&  \mathsf{true} \wedge \mathsf{true}.
\end{eqnarray*}
The next definition formalizes the construction and associates a characteristic formula to each partition.

\begin{definition}[Characteristic formula]
\label{formulas for partitions}
Let $W \subseteq \V$ be a finite set of variables, $\xi \in \Val(W)$ and $P \in \Pi(W)$. Then
\begin{eqnarray*}
\phi_P & \equiv & \left\{ \begin{array}{ll}
					\mathsf{true}	& \mbox{ if } W = \emptyset\\
					\phi_{\min(W),P} \wedge \phi_{P \lceil \{ \min(W) \}} & \mbox{ otherwise}
					\end{array} \right.
\end{eqnarray*}
where, for $x \in W$ and $B = [x]_P$,
\begin{eqnarray*}
\phi_{x, P} & \equiv & \left\{ \begin{array}{ll}
					\bigwedge_{C \in P - \{ B \} } x \neq \min(C) & \mbox{ if } B \mbox{ is a singleton}\\
					x = \min( B \setminus \{x \}) & \mbox{ otherwise }
				 \end{array} \right.
\end{eqnarray*}

\end{definition}


\begin{lemma}[Characteristic formula]
\label{partitions logic 2}
Let $W \subseteq \V$ be a finite set of variables, $\xi \in \Val(W)$ and $P \in \Pi(W)$. Then
$\xi \models \phi_P ~ \Leftrightarrow ~ P = \Part(\xi)$.
\end{lemma}
\begin{proof}
By induction on the number of elements of $W$.
\qed
\end{proof}


Below we recall the concept of a bisimulation. We will use bisimulations as notion of
equivalence between a register automaton and the associated right invariant register automaton.

\begin{definition}[Bisimulations]
Consider two Mealy machines with a common set $I$ of input symbols,
$\M_1 = \langle I, O_1, Q_1, q^0_1, \rightarrow_1 \rangle$ and
$\M_2 = \langle I, O_2, Q_2, q^0_2, \rightarrow_2 \rangle$.
Then we say that $\M_1$ and $\M_2$ are \emph{bisimilar} if there exists a bisimulation between their sets of states, that is,
a relation $U \subseteq Q_1 \times Q_2$ such that $(q^0_1, q^0_2) \in U$ and, whenever $(q_1, q_2) \in U$,
\begin{enumerate}
\item
$q_1 \xrightarrow{i/o}_1 q'_1$ implies there exists a transition $q_2 \xrightarrow{i/o}_2 q'_2$ such that $(q'_1, q'_2) \in U$,
\item
$q_2 \xrightarrow{i/o}_2 q'_2$ implies there exists a transition $q_1 \xrightarrow{i/o}_1 q'_1$ such that $(q'_1, q'_2) \in U$.
\end{enumerate}
It is easy to see that if $M_1$ and $\M_2$ are bisimilar they are equivalent, that is, they have the same traces.
Two register automata $\R_1$ and $\R_2$ are \emph{bisimilar} if $\sem{\R_1}$ and $\sem{\R_2}$ are bisimilar.
\end{definition}


We can now state the first main result of this section.
%TODO FV: Even if \R is input determinstic then \bar{\R} does not have to be input deterministic
%TODO How do we handle this? Is this a problem?
%TODO By Tm 1 if \R is input enabled then \bar{R} is input enabled
%TODO Ain't got it right yet
\fi
\fi
\begin{theorem}
\label{from RA to RIRA}
\ifverylong
For each register automaton $\R$ there exists a right invariant and unique-valued register automaton $\overline{\R}$ such that
$\R$ and $\overline{\R}$ are bisimilar.
\else
For each register automaton $\R$ there exists a right invariant and unique-valued register automaton $\overline{\R}$ such that
$\R$ and $\overline{\R}$ are equivalent.
\fi
%Moreover, if $\R$ is i/o determined then $\overline{\R}$ is i/o determined.
\end{theorem}
\iflong
\begin{proof}
\ifverylong
Let $\R=\langle I, O, L, l_0, V, \Gamma \rangle$ be a RA.
We have to construct a register automaton $\overline{\R}$ that is bisimilar to $\R$.
The basic idea behind our construction is to add to each location $l \in L$ a partition $P$
that puts two variables of $V(l)$ in the same block exactly when they have the same value. 
The registers of $\overline{\R}$ are then the minimal registers of the blocks of $P$.
Formally, $\overline{\R}=\langle I, O, \overline{L}, \overline{l}_0, \overline{V}, \overline{\Gamma} \rangle$, where
\begin{itemize}
\item
$\overline{L} = \{ (l, P) \mid l \in L \mbox{ and } P \in \Pi(V(l)) \}$,
\item
$\overline{l}_0 = (l_0, \emptyset)$,
\item
$\overline{V}(l, P) = \{ \min(B) \mid B \in P \}$,
\item
whenever $l \xrightarrow{i, g, \varrho, o} l'$ is a transition of $\Gamma$ and $Z \in \Pi(V(l) \cup \{ \In, \Out \})$ such that
$\phi_Z$ implies $g$,
then $\overline{\Gamma}$ contains a transition $(l, P) \xrightarrow{i, \overline{g}, \overline{\varrho}, o} (l', P')$, where
\begin{itemize}
\item
$P = Z \lceil \{ \In , \Out \}$,
\item
$P' = \varrho^{-1}(Z)$,
\item
$\overline{g} \equiv \phi_{\In,Z \lceil \{\Out \} } \wedge \phi_{\Out, Z}$, and
\item
for $x \in \overline{V}(l', P')$,  $\overline{\varrho}( x) = \min([\varrho(x) ]_Z )$.
\end{itemize}
(the reader may check that $\overline{g} \in \G(\overline{V}(l, P))$ and
$\overline{\varrho}( x) \in \overline{V}(l, P) \cup \{ \In, \Out \}$)
\end{itemize}
We verify that $\overline{\R}$ is right invariant by checking the conditions of Lemma~\ref{lemma right invariance}.
By Definition~\ref{formulas for partitions}, guard $\overline{g}$ is already in the restricted form of 
Lemma~\ref{lemma right invariance}, so condition 1 holds.
In order to check that $\overline{\varrho}$ is injective (condition 2), pick $x, y \in \overline{V}(l', P')$. We infer
\begin{eqnarray*}
\overline{\varrho}(x) = \overline{\varrho}(y) & \Leftrightarrow & (\mbox{definition } \overline{\varrho})\\
\min([\varrho(x) ]_Z ) = \min([\varrho(y) ]_Z ) & \Leftrightarrow & (\mbox{representatives equal iff their blocks equal})\\
\eqclass{\varrho(x)}_Z  = \eqclass{\varrho(y)}_Z  & \Leftrightarrow & (\mbox{use } P' = \varrho^{-1}(Z))\\
\eqclass{x}_{P'}  = \eqclass{y}_{P'}  & \Leftrightarrow & (\mbox{representatives equal iff their blocks equal})\\
x = y. & &
\end{eqnarray*}
For condition 3, suppose that $\phi_{\In,Z \lceil \{\Out \} } \equiv \In = x$, with $x \in V(l)$.
Then, by Definition~\ref{formulas for partitions}, $\In$ and $x$ are in the same block of $Z$.
But since $\overline{\varrho}$ maps variables in $\overline{V}(l', P')$ to representatives of blocks of $Z$, this means
it is impossible that $\overline{\varrho}$ maps one variable in $\overline{V}(l', P')$ to $\In$ and another to $x$.
Using a similar argument we may prove that condition 4 holds.

Let $U$ be the relation between states of $\sem{\R}$ and $\sem{\overline{\R}}$ defined as follows:
\begin{eqnarray*}
((l, \xi), ((l, P), \overline{\xi})) \in U & \Leftrightarrow & 
(P = \Part(\xi) \wedge \forall x \in \overline{V}(l,P) : \overline{\xi}(x) = \xi(x)).
\end{eqnarray*}
We claim that $U$ is a bisimulation relation. 
Since the sets of variables of the initial states of $\R$ and $\overline{\R}$ are empty,
the initial states of $\sem{\R}$ and $\sem{\overline{\R}}$ are trivially related by $U$.

For the first bisimulation transfer property,
suppose that $((l, \xi), ((l, P), \overline{\xi})) \in U$ and $(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')$.
Then this transition is supported by a transition $l \xrightarrow{i, g, \varrho, o} l'$ of $\Gamma$.
Hence, if $\iota = \xi \cup \{ (\In, d), (\Out, e) \}$ then $\iota\models g$ and $\xi' = \iota\circ\varrho$.
Let $Z = \Part(\iota)$. 
Then  $P = Z \lceil \{ \In , \Out \}$.
We claim that $\phi_Z$ implies $g$.
Because suppose $\iota' \models \phi_Z$, for some valuation $\iota'$.
Then, by Lemma~\ref{partitions logic 2}, $Z = \Part(\iota')$.
Since also $Z = \Part(\iota)$ and $\iota \models g$, we may conclude
$\iota' \models g$ by Lemma~\ref{partitions logic 3}, as required.
Let
$P' = \varrho^{-1}(Z)$,
$\overline{g} \equiv \phi_{\In,Z \lceil \{\Out \} } \wedge \phi_{\Out, Z}$, and,
for $x \in \overline{V}(l', P')$,  $\overline{\varrho}( x) = \min([\varrho(x) ]_Z )$.
Then, by definition,
$\overline{\Gamma}$ contains a transition $(l, P) \xrightarrow{i, \overline{g}, \overline{\varrho}, o} (l', P')$.
Let $\overline{\iota} = \overline{\xi} \cup \{ (\In, d), (\Out, e) \}$.
By Lemma~\ref{partitions logic 2}, $\iota \models \phi_Z$.
Since $\phi_Z$ implies $\overline{g}$ (here we benefit from our assumption that $\Out$ and $\In$ are minimal variables), 
$\iota \models \overline{g}$.
Now observe that $\overline{\iota}$ is just the restriction of $\iota$ to the minimal elements of its blocks together with
$\{ (\In, d), (\Out, e) \}$. Since these are the only variables that occur in $\overline{g}$, we conclude
$\overline{\iota} \models \overline{g}$.
Let $\overline{\xi}' = \overline{\iota} \circ \overline{\varrho}$.
Then, by Definition~\ref{semantics SMM}, $\sem{\overline{\R}}$ has a transition
$((l,P),\overline{\xi})\xrightarrow{i(d) / o(e)}((l', P'),\overline{\xi}')$.
We check that $((l', \xi'), ((l',P'), \overline{\xi}')) \in U$:
\begin{enumerate}
\item
By the definitions of $P'$ and $Z$, $P' = \varrho^{-1}(Z) = \varrho^{-1}(\Part(\iota))$.
By Definition~\ref{def partitions}, 
$\varrho^{-1}(\Part(\iota)) = \varrho^{-1}(\iota^{-1}(\Part(\integers))) = (\iota\circ\varrho)^{-1}(\Part(\integers))
= \Part(\iota\circ\varrho)$.
By the definition of $\xi'$, $\Part(\iota\circ\varrho) = \Part(\xi')$, as required.
\item
Let $x \in \overline{V}(l',P')$. Then 
$\overline{\xi}'(x) = \overline{\iota} \circ \overline{\varrho}(x) = \overline{\iota} (\min([\rho(x))]_Z)$.
Now the definition of partition $Z$ says that two variables in the same block are evaluated the same by $\iota$,
and it does not matter whether we take a minimal or any other element from the block.
Thus $\overline{\iota} (\min([\rho(x))]_Z) = \iota ( \rho (x))$.
By definition of $\xi'$, $ \iota ( \rho (x)) = \xi'(x)$.
Thus $\overline{\xi}'(x) = \xi'(x)$, as required.
\end{enumerate}

For the second bisimulation transfer property, suppose 
$((l, \xi), ((l, P), \overline{\xi})) \in U$ and 
$((l, P), \overline{\xi}) \xrightarrow{i(d) / o(e)}((l', P'), \overline{\xi}')$.
Then this transition is supported by some transition
$(l, P) \xrightarrow{i, \overline{g}, \overline{\varrho}, o} (l', P')$ and,
with $\overline{\iota} = \overline{\xi} \cup \{ (\In, d), (\Out, e) \}$,
we have $\overline{\iota} \models \overline{g}$ and
$\overline{\xi}' = \overline{\iota} \circ \overline{\varrho}$.
By definition of $\overline{\R}$, $\Gamma$ contains a transition
$l \xrightarrow{i, g, \varrho, o} l'$ and there exists a partition $Z \in \Pi(V(l) \cup \{ \In, \Out \})$ such that
\begin{itemize}
\item
$\phi_Z$ implies $g$,
\item
$P = Z \lceil \{ \In , \Out \}$,
\item
$P' = \varrho^{-1}(Z)$,
\item
$\overline{g} \equiv \phi_{\In,Z \lceil \{\Out \} } \wedge \phi_{\Out, Z}$, and
\item
for $x \in \overline{V}(l', P')$,  $\overline{\varrho}( x) = \min([\varrho(x) ]_Z )$.
\end{itemize}
Since $((l, \xi), ((l, P), \overline{\xi})) \in U$, $P = \Part(\xi)$.
By Lemma~\ref{partitions logic 2}, $\xi \models \phi_P$.
Let $\iota = \xi \cup \{ (\In, d), (\Out, e) \}$.
Then also $\iota \models \phi_P$.
Since $((l, \xi), ((l, P), \overline{\xi})) \in U$, $\xi$ is an extension of $\overline{\xi}$,
and thus also $\iota$ is an extension of $\overline{\iota}$.
Thus, since $\overline{\iota} \models \overline{g}$ also  $\iota\models \overline{g}$.
But since $P = Z \lceil \{ \In , \Out \}$, we may use $\phi_Z \equiv \overline{g} \wedge \phi_P$ to obtain 
$\iota \models \phi_Z$. Hence, since $\phi_Z$ implies $g$, $\iota \models g$.
Let $\xi' = \iota \circ \varrho$. Then by Definition~\ref{semantics SMM},
$(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')$.
We check that $((l', \xi'), ((l',P'), \overline{\xi}')) \in U$:
\begin{enumerate}
\item
Since $\iota \models \phi_Z$, Lemma~\ref{partitions logic 2} gives $Z = \Part(\iota)$.
Thus $P' = \varrho^{-1}(Z) = \varrho^{-1}(\Part(\iota))$.
By Definition~\ref{def partitions}, $\varrho^{-1}(\Part(\iota)) = \Part(\iota\circ\varrho)$.
By definition of $\xi'$, $\Part(\iota\circ\varrho) = \Part(\xi')$.
Thus $P' = \Part(\xi')$, as required.
\item
Identical to item (2) above.
\end{enumerate}
In order to see that $\overline{\R}$ is unique-valued, suppose that $((l, P), \overline{\xi})$ is a reachable
state of $\sem{\overline{\R}}$. Since $U$ is a bisimulation relation we may prove, by induction on the length of
the path to $((l, P), \overline{\xi})$, that there exists a state $(l, \xi)$ of $\sem{\R}$ such that
$((l, \xi), ((l, P), \overline{\xi})) \in U$.
Now suppose that $x, y \in \overline{V}(l, P)$ and $\overline{\xi}(x) = \overline{\xi}(y)$.
Then, using the definition of $U$, we may infer that $\xi(x) = \xi(y)$ and thus $x$ and $y$ are contained in the same block
of $P$.
But since $\overline{V}(l, P)$ consists of the minimal variables of all the blocks of $P$, this means that $x$ and $y$ are
in fact equal. We conclude that $\overline{\xi}$ is injective, as required.
\else
See the extended version of this paper available at \url{http://www.sws.cs.ru.nl/publications/papers/fvaan/TomteFresh/}.
\fi
\qed
\end{proof}
\fi

Cassel et al \cite{CasselHJMS15} established that right invariant register automata can be exponentially more
succinct than unique-valued register automata. 
The second main result of this section is that (arbitrary) register automata in turn can be exponentially more succinct 
than right invariant register automata.

\begin{theorem}
There exists a sequence of register automata $\R_1$, $\R_2$,.. such that the number of locations of $\R_n$ is $O(n)$,
but the minimal number of locations of a right invariant register automaton that is equivalent to $\R_n$ is $\Omega(2^n)$.
\end{theorem}
\iflong
\begin{proof}
The idea is to let $\R_n$ encode a binary counter with $n$ bits.
The register automaton $\R_n$ has input symbols $\init$ and $\tick$, 
output symbols $\OK$ and $\done$, 
$n+2$ locations $l_0, l_1, c_1 ,\ldots, c_n$, and
$n+2$ registers $\zero, \one, x_n ,\ldots, x_1$.
Figure~\ref{fig:counter} shows the transitions of $\R_n$.
We view $\tick$ as the default input symbol, $\OK$ as the default output symbol, and do
not display these default symbols in the diagram.
\begin{figure}[hbt]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.5cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [below of=1] {$l_1$};
  \node[state] (3) [below of=2] {$c_1$};
  \node[state] (4) [right of=3] {$c_2$};
  \node[state] (5) [right of=4] {$c_3$};
  \node[state, dashed] (6) [right of=5] {$~$};
  \node[state] (7) [right of=6] {$c_n$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=3cm] node {$\init(\In)$ \\ $\zero, x_n,\ldots, x_1 := \In$} (2)
    (2) edge [text width=1.5cm] node {$\In \neq \zero$ \\ $\init(\In)$\\ $\one := \In$} (3)
    (3) edge [text width=1.5cm] node {$x_1 = \one$\\ $x_1 := \zero$} (4)
        edge [loop left, text width=1.5cm] node {$x_1 = \zero$\\ $x_1 := \one$} (3)
    (4) edge [text width=1.5cm] node {$x_2 = \one$\\ $x_2 := \zero$} (5)
        edge [bend left, text width=1.5cm] node {$x_2 = \zero$\\ $x_2 := \one$} (3)
    (5) edge [bend left, text width=4.3cm] node { \hspace{2.8cm} $x_3 = \zero$\\ \hspace{2.8cm} $x_3 := \one$} (3)
        edge [dashed, text width=0.5cm] node {$~$} (6)
    (6) edge [dashed, text width=0.5cm] node {$~$} (7)
    (7) edge [bend right, text width=1.5cm] node {$x_n = \one$\\ $x_n := \zero$\\$\done$} (3)
        edge [bend left, text width=1.5cm] node {$x_n = \zero$\\ $x_n := \one$} (3);  
\end{tikzpicture}
\caption{Encoding a binary counter as a register automaton}
\label{fig:counter}
\end{figure}

Mealy machine $\sem{\R_n}$ has runs in which repeatedly location $c_1$ is visited.
The first time all the variables $x_n ,\ldots, x_1$ equal $\zero$, which encodes a binary counter with value $0$, with
$x_1$ representing the least significant bit.
Then, for each subsequent visit to $c_1$, the value of the counter is incremented by one.
When the counter overflows all the bits become $\zero$ again and an output $\done$ is generated. 
Since each cycle from $c_1$ to itself takes at least one transition,
it takes at least $2^n$ transitions before an output $\done$ occurs.
By Theorem~\ref{from RA to RIRA}, we know that there exists a right invariant register automaton that is equivalent to $\R_n$.
Let $\R'_n$ be such a right invariant register automaton with a minimal number of locations.
Then $\R'_n$ has a transition with output symbol $\done$, starting from a location $l$ that is reachable with
a cycle free path of transitions in $\R'_n$. 
Due to Corollary~\ref{Cy right invariance}, the number of transitions in this path is at least $2^n$ (otherwise
$\sem{\R'_n}$ would be able to produce an $\done$ prematurely).
Hence $\R'_n$ contains at least $2^n$ locations.
\qed
\end{proof}
\fi

In this article, we will present an algorithm for learning input enabled, input deterministic, register automata. 
The models produced by our learning algorithm will be a right invariant register automaton.
By the results of this section, such a right invariant register automaton will always exist, although it may be
exponentially less succinct than the original register automaton of the teacher.
%The following lemma states that such automata have a very specific form:

%\begin{lemma}
%Suppose $\R$ is an input enabled, input deterministic, right invariant register automaton and 
%assume w.l.og.\ that the guards of $\R$ satisfy the restrictions of Lemma~\ref{lemma right invariance}. 
%Suppose $l$ is a location that is reachable in $\R$, suppose $i$ is an input of $\R$, and suppose
%set $W$ consists of all variables $x$ for which $l$ has an outgoing $i$-transition with $g_{\In} \equiv \In = x$. Then:
%\begin{enumerate}
%\item
%$l$ has at most one outgoing $i$-transition with $g_{\In} \equiv \In = x$, for any $x$,
%\item
%if $l$ has two outgoing $i$-transitions with $g_{\In} \equiv \In = x$ and $g_{\In} \equiv \In = y$, for distinct $x$ and $y$,
%then $\xi \models x \neq y$, for each reachable state $(l,\xi)$ of $\sem{\R}$,
%\item
%$l$ has exactly one outgoing $i$-transition of the form $g_{\In} \equiv \bigwedge_{x \in W} \In \neq x$.
%\end{enumerate}
%\end{lemma}
%\iflong
%\begin{proof}
%By input determinism and input enabledness of $\R$.
%\qed
%\end{proof}
%\fi

%\begin{definition}
%Suppose guard $g \equiv g_{\In} \wedge g_{\Out}$ satisfies the restrictions of Lemma~\ref{lemma right invariance}. 
%We refer to $g_{\In}$ as the \emph{input guard} and to $g_{\Out}$ as the \emph{output guard}.
%We call in input guard \emph{closed} if it is of the form $\In = x$, and \emph{open} if it is of the form
%$\bigwedge_{x \in W} \In \neq x$.
%Similarly, we call in output guard \emph{closed} if it is of the form $\Out = x$, and \emph{open} if it is of the form
%$\bigwedge_{x \in W} \Out \neq x$.

%Suppose $\R$ is an input deterministic, right invariant register automaton in which all
%guards satisfy the restrictions of Lemma~\ref{lemma right invariance}. 
%We call a run of $\R$ \emph{farm-fresh} if, whenever $d$ is an input or output value that occurs in the run and
%the corresponding input resp.\ output guard is open, then $d$ is fresh.
%A trace of $\R$ is \emph{farm-fresh} if the unique run that corresponds to it is farm-fresh.
%\end{definition}
%
%\begin{lemma}
%Suppose $\R$ is an input deterministic, right invariant register automaton and 
%assume w.l.og.\ that the guards of $\R$ satisfy the restrictions of Lemma~\ref{lemma right invariance}. 
%Suppose that $\Out$ does not occur negatively in guards, that is, all open output guards are equal to $\mathsf{true}$.
%Then $\beta$ is a trace of $\R$ iff there exists farm-fresh trace $\beta'$ of $\R$ and a morphism $h$ such that
%$\beta = h(\beta')$.
%\end{lemma}
%\begin{proof}
%
%\end{proof}
%TODO What is complexity of deciding equivalence of RIRA?
