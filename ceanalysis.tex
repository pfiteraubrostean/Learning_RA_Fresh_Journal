\section{The Analyzer}
\label{sub:cex}
%This hypothesis does not check if the same value is inserted twice since the mapper only uses fresh values in the output queries.
During equivalence testing, a test generation component uses the abstract hypothesis to generate abstract test input sequences.
This approach allows us to use standard algorithms for FSM conformance testing such as 
Random Walk or a variation of the W-Method \cite{LeeY96}. 
These test sequences are then concretized, run on both the SUL and the concretized hypothesis, and the resulting outputs are compared. The result is either a concrete counterexample or increased confidence that the hypothesis model conforms to the SUL.

Parameter values in the abstract model can either be $\perp$ or a variable name. 
If an abstract value is a variable name then the corresponding concrete value is uniquely determined.
In contrast, an abstract value $\perp$ allows for infinitely many concretizations and suggests
that the SUL behaviour is independent of the value picked. By testing we can verify that this is the case.
If testing produces a counterexample then this may be used to refine the abstraction and introduce additional abstract values.  
To more quickly discover such refinements, we test by concretizing $\perp$ to different memorable values. 

%To more quickly discover such a refined abstraction, we test by concretizing according to the different types of refined abstractions possible. For each type, a probability is assigned with which Tomte selects a value corresponding to this type 
%To this end, we classify the possible concretization values for $\perp$ into memorable values, previous parameter values, constants, values in history and fresh values. Each class of values is assigned a probability with which Tomte selects a value from it. 
%With this probability Tomte chooses a class and randomly selects a value from it, value which then serves as the concretization. %In case a class of values is empty, its assigned probability is redistributed %uniformly across all remaining classes. 

As example, consider the login model of Figure~\ref{fig:Login2}.
Figure~\ref{fig:hypLogin} depicts the hypothesis built after the first iteration of learning this system.
\begin{figure}[htb]
\centering
\InputTikz{login2}
\caption{Initial abstract hypothesis for login system}
\label{fig:hypLogin}
\end{figure}
Using the testing approach described, Tomte will eventually find a concrete counterexample trace, say \login(9,9) \NOK \; \register(9,9) \OK \; \register(12,12) \NOK \; \login(9,9) \OK. This sequence is a valid trace of the SUL but not of the hypothesis, since according to the hypothesis the last output should be \NOK. 
Tomte applies heuristics to reduce the length of the counterexample,
in order to simplify subsequent analysis and thus to improve scalability. 
Two reduction strategies are used: (1) removing loops, and (2) removing single transitions.  
%
The first strategy tries to remove parts of the trace that form loops in the hypothesis. 
These may also form loops in the system and thus not affect the counterexample.
The second strategy tries to remove single transitions from the counterexample. 
The idea behind this is that often different parts of the system are independent of each other,
so transitions from the part not causing the counterexample can be removed.
%
Applied to the login case, Tomte first removes loops from the concrete counterexample, which results in the reduced counterexample \register(9,9) \OK\; \login(9,9) \OK. Tomte then tries to eliminate each transition, but as the resulting traces do not form counterexamples, this heuristics fails. As a final processing step, the counterexample is made neat, thus becoming  \register(0,0) \OK\; \login(0,0) \OK. This is done solely to improve the counterexample's readability. 

 %the input sequences \register(9,9), respectively \login(9,9), neither of which providing a counterexample.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.45]{ceanalysis}
\caption{Counterexample analysis in Tomte}
\label{fig:ceanalysis}
\end{figure}

The reduced counterexample is then analyzed by the process depicted in Figure~\ref{fig:ceanalysis}. The counterexample is first resolved by abstraction refinement. If no refinement can be done, then an abstracted form of the counterexample is sent to the Mealy Machine learner, which uses it to further refine the abstract hypothesis.

%The reduced counterexample is then analyzed by the process depicted in Figure~\ref{fig:ceanalysis}. Classical learning typically involves a series of counterexamples,
%thus a first test checks if the concrete counterexample induces an (abstract) counterexample for the underlying Mealy Machine learner, in which case it is served directly and the process stops. 
%In our running example, the corresponding abstract inputs would be \register($\perp$,$\perp$) \login($\perp$,$\perp$). These inputs induce the same abstract trace, both on the hypothesis and on the abstracted system, namely \register($\perp$,$\perp$) \OK() \login($\perp$,$\perp$) \NOK(). Hence no abstract counterexample can be formed, and Tomte needs to refine the input abstraction, a process we coin \textsl{abstraction refinement}. 
%
%Abstraction refinement consists of 4 chained sub-processes. 
Abstraction refinement means finding the concrete input parameters that are abstracted to $\perp$ but nevertheless form 'relevant' relations
with previous parameters. We say that a relation between two parameters is relevant if breaking it also breaks the counterexample. 
Consequently, the concrete value of these parameters no longer fits $\perp$, as they can only take a specific value for the counterexample to hold.
Based on relevant relations, we then update the lookahead oracle and construct refined abstractions, that would better fit these parameters. Initially,
all parameters values are abstracted to $\perp$. This changes as more refined abstractions are created.
%At start,
%the only available abstraction is $\perp$, hence all parameters in the login counterexample bear $\perp$ abstractions. So all are viable candidates


A first step to refining is disambiguation, by which any relations between two parameters present that are not relevant for the counterexample, are broken by replacing the latter parameter of the relation with a fresh value.  In our running example the trace \register(0,0) \OK\; \login(0,0) \OK\; is changed to \register(0,1) \OK\; \login(0,1) \OK, by virtue of the irrelevant equality between
the username and password. Breaking relations further would change the observed behavior into one with which the concrete hypothesis would agree.


%Intuitively, any value should be remembered (or is memorable) from its first occurrence to its last occurence in the disambiguated counterexample. In case it isn't 

%More abstractly From this we can conclude that a value must be at least be memorable from the first occurrence to its last occurence in the counterexample. So currently the lookahead traces misses these 'missing' memorable values, and to solve it we have to add new lookahead traces.

The disambiguated trace is then sent to the next process, which looks for any missing memorable values and adapts the lookahead oracle so these can all be discovered. The current memorable values are obtained by running the counterexample through the lookahead oracle, which then decorates the trace by placing memorable value lists at the start and after each transition. Such a trace for the login case would be $\epsilon \;\register(0,1) \; \OK \; \epsilon\; \login(0,1)\; \OK \;\epsilon$. Notice that all the sequences are empty, since initially the lookahead oracle does not find any memorable values. For the last output to be \OK, the SUL requires that values 0 and 1 are reused in the \login-input, meaning that the SUL should have remembered them, hence these values should have been found memorable by the lookahead oracle. We say that the lookahead oracle `misses' these values. In more concrete terms, we say that a parameter value is missing if it is  equal to a value from a previous transition, but not contained in the list of memorable values that directly precedes the transition. For the login example, we notice that both 0 and 1 appear as missing values in \login(0,1), since they first emerged in the \register{} action but they were not included in the memorable set before \login . 

The process iterates over the input actions of the decorated trace. Once it passes by an input parameter whose value is judged to be missing, it builds a symbolic lookahead trace that would allow the lookahead oracle to uncover this value. The counterexample is then re-decorated through the augmented lookahead oracle and iteration continues with the next parameter. The end result is a decorated trace 
which contains no missing values. For the login case, the process updates the lookahead oracle and re-decorates the trace for each of \login's parameters. The end result is the decorated trace where both 0 and 1 are no longer missing:
$ \epsilon \; \register(0,1) \; \OK \; [0,1] \; \login(0,1) \; \OK \; \epsilon$.

%We say that a parameter's value is missing if it is (1) equal to a past value; (2) not contained in the memorable set before the
%last transition; and (3) not a special value (or constant). For the login case, we notice that both 0 and 1 appear as lost values in \login(0,1), since they first emerged in the \register action yet they weren't included in the memorable set before \login . Both 0 and 1 have been lost after the \register(0,1) \OK() transition, as such the lookahead trace is built from this point on to the end of the counterexample, namely from the trace \login(0,1) \OK() . For each value, a symbolic lookahead trace is built and added to the lookahead oracle. The trace is re-run on the system again through the lookahead oracle. After analyzing the value 0 and re-running, the trace takes the configuration []\register(0,1) \OK() [0] \login(0,1) \OK() []. Notice, 0 is no longer lost after \register(0,1)\OK(). Similarly, after processing of the second value, the trace obtains the configuration [] \register(0,1) \OK() [0,1] \OK() [0,1] \login(0,1) \OK() []. This decorated trace is free of lost values and is passed on to the next process. The actual symbolic lookahead traces added to augment the lookahead oracle for this particular case are \login(L0,l1) for 0, respectively \login(l0,L1) for 1. 

A trace decorated with all memorable values is then sent to the next process, which further decorates the trace so that each concrete value is paired with its corresponding abstract value. 
This  is achieved by running the counterexample through both the mapper (which adds the abstractions) and the
lookahead oracle (which adds the memorable values). 
In the login example, as initially $\perp$ is the only abstract value available, decoration results in the trace 
\[
\epsilon \;  \register(0:\perp,1:\perp) \;   \OK\; [0,1]\; \login(0:\perp,1:\perp)\; \OK \; \epsilon. 
\]
This trace is then iterated and whenever (1) a concrete value is equal to a memorable value, and (2) the corresponding abstraction is $\perp$, a new abstract value is created for the corresponding input symbol and the mapper is updated accordingly. Equality with a memorable value results in an abstraction which simply points to an index in the memorable value list after the previous transition. 
In the login example, the new abstraction values for the \login-action are $w_1$ for the first parameter, respectively $w_2$ for the second, transforming the decorated trace into 
\[
\epsilon \; \register(0:\perp,1:\perp) \; \OK\;[0,1] \; \login(0:w_1,1:w_2)\; \OK \;\epsilon. 
\]

The mechanisms of uncovering missing memorable values and new abstractions are closely tied to proper disambiguation of the counterexample. Both these steps consider any equalities between two parameters as relevant to the counterexample. Applying the same process on an ambiguous counterexample might result in resolution of false relations or missing relations which are confounded 
as was in the login case. Without disambiguation, the counterexample 
$\register(0,0) \; \OK\; \login(0,0)$ $\OK$ would have yielded only one missing value in 0, which would have lead to different
refined abstractions. One such abstraction would imply that it is relevant if the second \register{} parameter is equal to the first, which is clearly not the case. 
%one lookahead trace and three abstractions, one $p_0$ for \register{}'s second parameter where $p_0$ points to the first parameter, two $x_0$ abstractions for each of \login{}'s parameters.


The final step of the counterexample analysis is a simple check if new lookahead traces or new abstract values have been
discovered during the last pass. If so, learning is restarted from scratch. 
Note that memorable values discovered by newly added lookahead traces can have corresponding abstract values which have already been created as a result of a previous refinements. Or the abstract values found might expose relations with previous input values. Similarly, it may happen that the lookahead oracle has already discovered all memorable values, yet for some of these values new abstract values are defined. Learning needs to be restarted as LearnLib currently does not accept on the fly changes to the input alphabet. 
Moreover, some of the answers to queries from the learning phase might be invalidated by the discovery of new memorable values.

If no new lookahead traces or abstract values have been discovered during a pass, then an abstract version of
the counterexample is forwarded to the Mealy machine learner.
%These situations
%are a consequence of the global nature of symbolic lookahead traces and refined abstractions respectively. 
%This is due to the global nature of the lookahead oracle, a symbolic lookahead trace might uncover memorable values in multiple scenarios. 
%Abstraction refinement is not always successful in which case the counterexample is abstracted and delivered to the classical (Mealy Machine) learner. Intuitively, classical learning involves
%iterative steps in which hypothesis are formed and then refined over a series of abstract counterexamples. In our setting, traces which induce such counterexamples do not lead to abstraction refinement 
%and should only be supplied to the learner. 
Obtaining an abstract counterexample involves just running the counterexample through the mapper and lookahead oracle and only collecting the abstracted messages. As an optimization, we also perform this step before abstraction refinement, as it is a considerably cheaper yet just as likely. 
%A pertinent question is whether it can happen that a counterexample has no refinement, yet does not induce an abstract counterexample. A case is when the system

According to Figure~\ref{fig:ceanalysis}, counterexample analysis in Tomte has three possible outcomes:
(1) a new lookahead trace is forwarded to the Lookahead Oracle and learning is restarted,
(2) a new abstract value is forwarded to the Abstractor and learning is restarted, or
(3) a counterexample is forwarded to the learner.
By Lemma~\ref{lemma lookahead automorphisms}, step (1) may only occur a finite number of times.
Since the number of input symbols and the number of abstract values are both finite, also step (2) may only occur a
finite number of times.
If there are no more steps of type (1) or type (2) then, 
by Theorem~\ref{theorem abstractor} and Theorem~\ref{theorem myhill nerode}, the set of abstract traces 
that can be observed by the learner equals the set of traces of some finite, deterministic Mealy machine.
By correctness of the Mealy machine learner, the learner will produce a correct hypothesis after a finite
number of queries.
Thus we may conclude that our algorithm for learning register automata terminates.