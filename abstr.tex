\subsection{Mappers}
\label{sec:predabstr}

Below we recall relevant parts of the theory of mappers from \cite{AJUV15}.
In order to learn an over-approximation of a  ``large'' Mealy machine $\M$,
we may place a transducer in between the teacher and the learner,
which translates concrete inputs to abstract inputs,
concrete outputs to abstract outputs, and vice versa.
This allows us to reduce the task of the learner to inferring a ``small'' Mealy machine with an abstract alphabet.
As we will see, the determinizer and the abstractor of Figure~\ref{fig:learningComponents} are examples of such transducers.

The behavior of a transducer is fully specified by a \emph{mapper},
a deterministic Mealy machine in which concrete actions are inputs and abstract actions are outputs.

\begin{definition} [Mapper]
\label{def:abstr}
A \emph{mapper} is a deterministic Mealy machine $\A = \langle I \cup O, X \cup Y,  R, r_0, \delta, \abstr \rangle$, where
\begin{itemize}
\item $I$ and $O$ are disjoint sets of {\em concrete input and output actions},
\item $X$ and $Y$ are disjoint sets of {\em abstract input and output actions}, and
\item $\abstr : R \times (I \cup O) \rightarrow (X \cup Y)$, referred to as the \emph{abstraction function},
respects inputs and outputs, that is, for all $a \in I \cup O$ and $r \in R$, $a \in I \Leftrightarrow \abstr(r,a) \in X$.
\end{itemize}
\end{definition}
A mapper $\A$ translates any sequence $\beta \in (I \cup O)^{\ast}$ of concrete actions into a corresponding
sequence of abstract actions given by
\begin{eqnarray*}
\ABS{\beta}{\A} & = & \lambda(r_0, \beta).
\end{eqnarray*}
A mapper also allows us to abstract a Mealy machine with concrete actions in $I$ and $O$ into
a Mealy machine with abstract actions in $X$ and $Y$.
Basically, the \emph{abstraction} of Mealy machine $\M$ via mapper $\A$ is the Cartesian product of the underlying
transition systems, in which the abstraction function is used to convert concrete actions into abstract ones.

\begin{definition} [Abstraction]
\label{def:Abstraction}
Let $\M = \langle I, O, Q, q_0, \rightarrow \rangle$ be a Mealy machine and let
$\A = \langle I \cup O, X \cup Y, R, r_0, \delta, \abstr \rangle$ be a mapper.
Then $\ABS{\M}{\A}$, the \emph{abstraction of $\M$ via $\A$}, is the Mealy machine
$\langle X, Y \cup \{ \perp \}, Q \times R, (q_0 , r_0), \rightarrow \rangle$, where $\perp \not\in Y$ is a fresh output action and
$\rightarrow$ is given inductively by the rules
\[
\frac{q \xrightarrow{i/o} q' ,~ r \xrightarrow{i/x} r' \xrightarrow{o/y} r''}
{(q, r) \xrightarrow{x/y} (q',r'')}
\hspace{1cm}
\frac{\not\exists i \in I : r \xrightarrow{i/x}}
{(q, r) \xrightarrow{x/\perp} (q,r)}
\]
\end{definition}
The first rule says that a state $(q,r)$ of the abstraction has an outgoing $x$-transition for each
transition $q \xrightarrow{i/o} q'$ of $\M$ with $\abstr(r,i) =x$.
In this case, there exist unique $r'$, $r''$ and $y$ such that $r \xrightarrow{i/x} r'$ and $r' \xrightarrow{o/y} r''$.
An $x$-transition in state $(q,r)$ then leads to state $(q', r'')$ and produces output $y$.
The second rule in the definition ensures that the abstraction $\ABS{\M}{\A}$ is input enabled.
Given a state $(q,r)$ of the mapper, it may occur that for some abstract input $x$ there does not
exist a corresponding concrete input $i$ with $\abstr(r,i) = x$.
In this case, an input $x$ triggers the special ``undefined'' output action $\perp$ and leaves the state unchanged.

\begin{lemma}
Let $\A$ be a mapper and let $\M$ be a Mealy machine with the same concrete input and output actions $I$ and $O$.
If $\beta$ is a trace of $\M$ then $\ABS{\beta}{\A} $ is a trace of $\ABS{\M}{\A}$.
\end{lemma}
\iflong
\begin{proof}
Straightforward, see also Lemma 4 of \cite{AJUV15}.
\qed
\end{proof}
\fi

A mapper describes the behavior of a transducer component that we can place in between a Learner and a Teacher.
Consider a mapper $\A = \langle I \cup O, X \cup Y, R, r_0, \delta, \abstr \rangle$.
The transducer component that is induced by $\A$ records the current state, which initially is set to $r_0$, and behaves as follows:
\begin{itemize}
\item
Whenever the transducer is in a state $r$ and receives an abstract input $x \in X$ from the learner,
it nondeterministically picks a concrete input $i \in I$ such that $\abstr(r,i)=x$,
forwards $i$ to the teacher, and jumps to state $\delta(r,i)$.
If there exists no such input $i$, then the component returns output $\perp$ to the learner.
\item
Whenever the transducer is in a state $r$ and receives a concrete answer $o$ from the teacher, it forwards
$\abstr(r,o)$ to the learner and jumps to state $\delta(r,o)$.
\item
Whenever the transducer receives a reset query from the learner,
it changes its current state to $r_0$, and forwards a reset query to the teacher.
\end{itemize}
From the perspective of a learner, a teacher for $\M$ and a transducer for $\A$ together behave exactly like a teacher for $\ABS{\M}{\A}$.
(We refer to \cite{AJUV15} for a formalization of this claim.)
In \cite{AJUV15}, also a \emph{concretization} operator $\gamma_{\A}$ is defined.
Let $\hypo$ be a Mealy machine with ``abstract'' actions in $X$ and $Y$.
The concretization operator $\gamma_{\A}$ is the adjoint of the abstraction operator:
it turns $\hypo$ into a concrete Mealy machine $\CONC{\A}{\hypo}$ with actions in $I$ and $O$.
As shown in \cite{AJUV15}, $\ABS{\M}{\A} \leq \hypo$ implies $\M \leq \CONC{\A}{\hypo}$.
