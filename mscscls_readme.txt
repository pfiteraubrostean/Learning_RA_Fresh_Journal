% README.TXT for mscs.cls
% v1.0 released 20th August 1997
% Copyright 1997 Cambridge University Press
%
% This software may only be used in the preparation of journal articles
% or books or parts of books to be published by Cambridge University Press.
% Any other use constitutes an infringement of copyright.
%
% The files for mscs.cls are distributed in a concatenated form (using the
% LaTeX 2e 'filecontents' environment). The file 'mscscls.ltx' is one of these
% files. The 'filecontents' environment allows the files to be unpacked easily
% with the correct filenames.
%
% To unpack the source files, copy the mscscls.ltx file to a temporary
% directory. Then pass the mscscls.ltx file though LaTeX 2e as if it were a
% document (i.e. TeX it with LaTeX 2e).  LaTeX will then unpack the files.
%
% mscscls.ltx contains the following files:
%
%   mscs.cls        the class file
%   mscs2egu.tex    authors' instructions
%
% If LaTeX 2e refuses to unpack the files, you probably already have
% them installed (in the TeXinput path). In this case, if the supplied files
% are newer, we strongly  recommend that you use the newer files. To do this
% rename any existing file and unpack the distribution again.
%
% Once you have unpacked the files, move the *.cls file to a directory in
% the TeXinput path. Then typeset the guide (mscs2egu.tex) for instructions
% and examples.
