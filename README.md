This repo is for a journal entry aiming at unifying the ICTAC 2015 paper  
with related theoretical elements introduced by Judith's Master's dissertation.


The ICTAC paper presented a new iteration of Tomte which enabled Tomte to  
learn Register Automatas with fresh value generation.


Judith van Steger for her dissertation worked on proving properties of both  
some of the new elements introduced by the iteration as well as some of  
the pre-existing ones. Namely:   
1. proved correctness of the determinizer component, an added feature
in Tomte
2. proved an upper bound on the size of the lookahead trace  

This work aims to unify the two articles. The target journal is MSCS:
http://journals.cambridge.org/jid_MSC