
\section{The Lookahead Oracle}
The main task of the lookahead oracle is to compute for each trace of the SUL a set of values
that are memorable after occurrence of this trace. Intuitively, a value $d$ is memorable if 
it has an impact on the future behavior of the SUL: either $d$ occurs in a future output, or a future 
output depends on the equality of $d$ and a future input.
The notion of a memorable value is fundamental for register automata and was previously studied e.g.\  in \cite{BergJR08}.

\begin{definition}
\label{def:memorableValue}
Let $\R$ be a register automaton,
let $\beta$ be a trace of $\R$, and
let $d \in \integers$ be a parameter value that occurs in $\beta$.
Then $d$ is \emph{memorable after} $\beta$ iff there exists a \emph{witness} for $d$, that is,
a sequence $\beta'$ such that $\beta \; \beta'$ is a trace of $\R$ and
if we replace each occurrence of $d$ in $\beta'$ by a fresh value $f$
then the resulting sequence $\beta ~ (\beta' [f/d])$ is not a trace of $\R$ anymore.
\end{definition}

\begin{example}
In the example of Figure~\ref{fig:fifoSet}, the set of memorable values after trace
$\beta = \push(1) ~ \OK ~ \push(2) ~ \OK ~ \push(3) ~ \NOK$ is $\{ 1, 2 \}$.
Values $1$ and $2$ are memorable, because of the witness $\beta' = \pop ~ \oout(1) ~ \pop ~ \oout(2)$.
Sequence $\beta \; \beta'$ is a trace of the model, but if we rename either the $1$ or the $2$ in $\beta'$ into a fresh value,
then  this is no longer the case.
In the example of Figure~\ref{fig:login}, value 2207 is memorable after $\register \; \OK(2207)$
because $\register ~ \OK(2207) ~ \login(2207)$ $\OK$ is a trace of the automaton, 
but $\register ~ \OK(2207) ~ \login(1) ~ \OK$ is not.
\end{example}

The next theorem gives a state based characterization of memorable values: a value $d$ is memorable
after a run of a deterministic register automaton iff the final state of that run is inequivalent to
the state obtained by replacing all occurrences of $f$ by a fresh value.
Thus we can also say that a value $d$ is memorable in a state of a register automaton.

\begin{theorem}
\label{theorem memorable}
Let $\R$ be a deterministic register automaton, let $\alpha$ be a run of $\M$ with $\trace(\alpha) = \beta$, 
let $(l, \xi)$ be the last state of $\alpha$, let $d \in\integers$, and let $f \neq d$ be a fresh
value that does not occur in $\alpha$.
Let $\swap_{d,f}$ be the automorphism that maps $d$ to $f$, $f$ to $d$, and acts as identity for all other values.
Then $d$ is memorable after $\beta$ iff $(l, \xi) \not\approx (l, \swap_{d,f} (\xi))$.
\end{theorem}
\iflong
\begin{proof}
Suppose $d$ is memorable after $\beta$.
Then there exists a witness for $d$, that is,
a sequence $\beta'$ such that $\beta \; \beta'$ is a trace of $\R$ and
$\beta ~ \swap_{d,f}(\beta')$ is not a trace of $\R$.
Since $\R$ is deterministic, $\alpha$ is the unique run of $\M$ with $\trace(\alpha) = \beta$.
Therefore, since $\beta \; \beta'$ is a trace of $\R$, there exists a partial run $\alpha'$ that starts in $(l, \xi)$
such that $\trace(\alpha') = \beta'$.
Moreover, since $\beta ~ \swap_{d,f}(\beta')$ is not a trace of $\R$,
$\swap_{d,f}(\beta')$ is not a trace of $(l, \xi)$.
By Lemma~\ref{automorphism preserves runs}, $\swap_{d,f} (\alpha')$ is a partial run of $\R$ that
starts in $(l, \swap_{d,f} (\xi))$.
By Lemma~\ref{trace and automorphism commute}, $\trace(\swap_{d,f}(\alpha')) = \swap_{d,f} (\beta')$.
Thus $\swap_{d,f} (\beta')$ is a trace of $(l, \swap_{d,f} (\xi))$, which in turn implies
$(l, \xi) \not\approx (l, \swap_{d,f} (\xi))$.

For the other direction, suppose $(l, \xi) \not\approx (l, \swap_{d,f} (\xi))$.
Then there exists a sequence $\beta'$ that is a trace of $(l, \xi)$ but not of $(l, \swap_{d,f} (\xi))$.
We claim that $\beta'$ is a witness for $d$.
Clearly, $\beta ~ \beta'$ is a trace of $\R$.
Now suppose $\beta ~ \swap_{d,f}(\beta')$ is a trace of $\R$.
Then, since $\R$ is deterministic, $\swap_{d,f}(\beta')$ is a trace of $(l, \xi)$.
By Lemmas~\ref{automorphism preserves runs} and \ref{trace and automorphism commute},
$\swap_{d,f}( \swap_{d,f}(\beta'))$ is a trace of $(l, \swap_{d,f}(\xi))$.
Therefore, since $\swap_{d,f}$ is its own inverse, $\beta'$ is a trace of $(l, \swap_{d,f}(\xi))$,
and we have derived a contradiction.
Thus our assumption was wrong and $\beta ~ \swap_{d,f}(\beta')$ is not a trace of $\R$.
\qed
\end{proof}
\fi

The above theorem reduces the problem of deciding whether a value is memorable to
the problem of deciding equivalence of two states in a register automaton.
It is not hard to see that conversely the problem of deciding equivalence of states
can be reduced to the problem of deciding whether a value is memorable.
The problem of finding a witness for a memorable value is thus equivalent to the problem of finding a
distinguishing trace between two states.

%In order to simplify the presentation, I just say here that the Lookahead Oracle annotates each output with a
%set of memorable values, rather than a valuation.
%(So we are lying a bit.)
%At the end of the section, after explanation of the concept of lookeahead traces, I will say that valuations
%rather than sets of values are added.
Consider the architecture of Figure~\ref{fig:learningComponents}.
Whenever the Lookahead Oracle receives an input from the Abstractor, this is just forwarded to the Determinizer.
However, when the Lookahead Oracle receives a concrete output $o$ from the Determinizer,
then it forwards $o$ to the Abstractor, together with a list of the memorable values after the occurrence of $o$.
The ordering of the memorable values in the list determines in which registers the values will be stored by the Abstractor.
Different orderings are possible, and the choice of the ordering affects the size of the register automaton that we
will learn (similar to the way in which the variable ordering affects the size of a Binary Decision Diagram \cite{Bry86}).
Within the Tomte tool we have experimented with different orderings. A simple way to order the values, for instance, is to sort 
them in ascending order. An ordering that works rather well in practice, and on which we elaborate below, is the order in which the values occur in the run.

Let $\R$ be the input deterministic register automaton that we want to learn, and let $\beta$ be a trace of $\R$. 
Then, since $\R$ is input deterministic, it has a unique run
\[
\alpha = (l_0, \xi_0) ~ i_0 (d_0) ~ o_0(e_0) ~(l_1, \xi_1) ~ i_1 (d_1) ~ o_1 (e_1) ~ (l_2, \xi_2) \cdots
\]
\[
\hspace{4cm} \cdots  ~ i_{n-1} (d_{n-1}) ~ o_{n-1} (e_{n-1}) ~ (l_n, \xi_n).
\]
such that $\trace(\alpha) = \beta$.
For $j \leq n$, we define $r_j \in \integers^{\ast}$ inductively as follows: $r_0 = \epsilon$ and, for $j>0$,
$r_j$ is obtained from $r_{j-1}$ by first appending $d_{j-1}$ and/or $e_{j-1}$ in case these values do not occur in the
sequence yet, and then erasing all values that are not memorable in state $(l_j, \xi_j)$.
Then the task of the Lookahead Oracle is to annotate each output action of $\beta$ with the list of memorable values of the
state reached by doing this output:
\begin{eqnarray*}
\Oracle_{\R}(\beta) & = & i_0 (d_0) ~ o_0(e_0 r_1) ~ i_1 (d_1) ~ o_1 (e_1 r_2) \cdots i_{n-1} (d_{n-1}) ~ o_{n-1} (e_{n-1} r_n).
\end{eqnarray*}

In order to accomplish its task, the Lookahead Oracle stores all the traces of the SUL observed during learning in an \emph{observation tree}.

\begin{definition}
An \emph{observation tree} is a pair $(\N, \MemV)$, where $\N$ is a finite, nonempty, prefix-closed set of collision free, neat traces, and function $\MemV : \N \rightarrow \integers^{\ast}$ associates to each trace a finite
sequence of distinct values which are memorable after running this trace.
\end{definition}
In practice, observation trees are also useful as a cache for repeated queries on the SUL. 
Figure~\ref{fig:OTfifoSet} shows two observation trees for our FIFO-set example.
For each trace $\beta_j$ a list of memorable values is given.

\begin{figure}[ht]

\begin{subfigure}[t]{0.45\textwidth}
        \InputTikz{OTfifoSet2}%
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
  %(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[t]{0.45\textwidth}
        \InputTikz{OTfifoSet3}
\end{subfigure}
\caption{Observation trees for FIFO-set without and with $\pop$ lookahead trace  }
\label{fig:OTfifoSet}%
\end{figure}
Whenever a new trace $\beta$ is added to the tree, the oracle computes a list of memorable values for it.
For this purpose, the oracle maintains a list $L = \langle \sigma_1 ,\ldots, \sigma_k \rangle$
of \emph{lookahead traces}. 
These lookahead traces are run in sequence after $\beta$ to explore the future of $\beta$ and 
to discover its memorable values. 
%(Since we can always reset the SUL and since the Determinizer creates a behavior deterministic view of the SUL,
%we can return to $N$ as often as we want to run experiments.)

\begin{definition}
A \emph{lookahead trace} is a sequence of symbolic input actions of the form $i(v)$ with $i \in I$ and
$v \in \{ p_1, p_2,\ldots \} \cup \{ n_1, n_2,\ldots \} \cup \{ f_1 , f_2,\ldots \}$.
\end{definition}
Intuitively, a lookahead trace is a symbolic trace, where each parameter refers to either 
a previous value ($p_j$), or 
to a fresh input value ($n_j$), or 
to a fresh output value ($f_j$). 
Within lookahead traces, parameter $p_1$ plays a special role as the parameter that is replaced by a fresh value.
Let $\sigma$ be a lookahead trace in which parameters $P$ refer to previous values,
and let $\zeta$ be a valuation for $P$.
Then $\sigma$ can be converted into a concrete trace on the fly, by replacing each variable
$p_j \in P$ by  $\zeta(p_j)$, picking a fresh value for each variable $n_j$ whenever needed, and assigning to $f_j$
the $j$-th fresh output value.
If trace $\gamma$ is a possible outcome of converting lookahead trace $\sigma$, 
starting from a state $(l, \xi)$ with valuation $\zeta$,
then we say that $\gamma$ is a \emph{concretization} of  $\sigma$. 

The following lemma implies that a finite number of lookahead traces will suffice to discover
all memorable values of all states in an observation tree. The idea is that if a concretization of a lookahead trace is a witness
that a value is memorable in some state, the same lookahead trace can also be used to discover that a corresponding value is
memorable in any symmetric state.

\begin{lemma}
\label{lemma lookahead automorphisms}
Let $\R$ be a register automaton and let $(l, \xi)$ be a state of $\sem{\R}$.
Let $\sigma$ be a lookahead trace in which parameters $P = \{ p_1 ,\ldots, p_l \}$ refer to previous values,
and let $\zeta$ be a valuation that assigns to each parameter in $P$ a distinct memorable value of $(l, \xi)$.
Suppose $\gamma$ is a concretization of $\sigma$ starting from $(l, \xi)$ with valuation $\zeta$, and
suppose $\gamma$ is also a witness showing that $\zeta(p_1)$ is memorable in state $(l, \xi)$.
Let $h$ be an automorphism and
suppose $\gamma'$ is a concretization of $\sigma$ starting from state $h(l, \xi)$ with valuation $h \circ\zeta$.
Then $\gamma'$ is a witness showing that $h(\zeta(p_1))$ is memorable in state $h(l, \xi)$.
\end{lemma}

If $M$ is an overapproximation of the set of memorable values after some state $(l', \xi')$ then, by
concretizing lookahead trace $\sigma$ for each injective valuation in $P \rightarrow M$,
Lemma~\ref{lemma lookahead automorphisms} guarantees that we will find a witness in case
there exists an automorphism $h$ from $(l, \xi)$ to $(l', \xi')$.

Instances of all lookahead traces are run in each new node to compute memorable values. At any point in time, 
the set of values that occur in $\MemV(\beta)$ is a subset of the full set of memorable values of node $\beta$.
Whenever a memorable value has been added to the observation tree, we require the tree to be \emph{lookahead complete}. 
This means every memorable value has to have an origin, that is, 
it has to stem from either the memorable values of the parent node
or the values in the preceding transition:
\begin{eqnarray*}
\beta' = \beta \; i(d) \; o(e)  & \Rightarrow & \values(\MemV(\beta')) \subseteq \values(\MemV(\beta)) \cup \{ d, e \},
\end{eqnarray*}
where function $\values$ returns the set of elements that occur in a list.
We employ a similar restriction on any non-fresh output parameters contained in the transition leading up to a node. These too have to originate 
from either the memorable values of the parent, or the input parameter in the transition. Herein we differentiate from the algorithm in~\cite{ThesisFides}
which only enforced this restriction on memorable values at the expense of running additional lookahead traces. %* could give intuition%
%example with of simple FIFO%

The observation tree at the left of Figure~\ref{fig:OTfifoSet} is not lookahead complete since 
output value $1$ of action \oout(1) is
neither part of the memorable values of the node $\beta_1$ nor is it an input in $\pop$.
Whenever we detect such an incompleteness, we add a new lookahead trace (in this case $\pop$) and restart the entire
learning process with the updated set of lookahead traces to retrieve a lookahead complete observation tree. 
The observation tree at the right is constructed after adding the lookahead trace $\pop$. 
This trace is executed for every node constructed, as highlighted by the dashed edges. 
The output values it generates are then tested if they are memorable and if so, stored in the $\MemV$ set of the node. 
When constructing node $\beta_1$,  the lookahead trace $\pop$ gathers the output $1$. 
This output is verified to be memorable and then stored in $\MemV(\beta_1)$. 
We refer to \cite{ThesisFides} for more details about algorithms for the lookahead oracle.

\section{The Abstractor}
The task of the abstractor is to rename the large set of concrete values of the SUL to
a small set of symbolic values that can be handled by the learner.

Let $w_0 , w_1, \ldots$ be an enumeration of the set $\V \setminus \{ \In, \Out \}$. If the SUL can be described
by a register automaton in which each location has at most $n$ variables, then the abstract values used by
the abstractor will be contained in $\{ w_0,\ldots, w_{n-1}, \perp \}$.
We define a family of mappers $\A_F$, which are parametrized by a function $F$
that assigns to each input symbol a finite set of variables from $\V \setminus \{ \In, \Out \}$.
Intuitively, $w \in F(i)$ indicates that it is relevant whether the parameter of input symbol $i$ is equal to $w$ or not.
The initial mapper is parametrized by function $F_\emptyset$ that assigns to each input symbol the empty set.
Using counterexample-guided abstraction refinement, the sets $F(i)$ are subsequently extended.

The states of $\A_F$ are injective sequences of values
(that is, sequences in which each value occurs at most once), 
with the initial state being equal to the empty sequence.
A sequence $r = d_0 \ldots d_{n-1} \in\integers^{\ast}$ represents the valuation 
$\xi_r$ for $\{ w_0,\ldots, w_{n-1} \}$ given by $\xi_r(w_j) = d_j$, for all $j$.
Note that $r$ is injective iff $\xi_r$ is injective.
The abstraction function of mapper $\A_F$ leaves the input and output symbols unchanged, but modifies the parameter values.
The actual value of an input parameter is replaced by the variable in $F(i)$ that has the same value,
or by $\perp$ in case there is no such variable.
Thus the abstract domain of the parameter of $i$ is the finite set $F(i) \cup \{ \perp \}$.
Likewise, the actual value of an output parameter is not preserved, but only the name of the variable
that has the same value, or $\perp$ if there is no such variable.
The (injective) sequence $r'$ of memorable values that has been added as an annotation by the lookahead oracle 
describes the new state of the mapper after an output action.
The abstraction function replaces $r'$ by an update function $\varrho$ that specifies how $r'$
can be computed from the old state $r$ and the input and output values that have been received.
Upon receipt of a concrete output $o(e ~ r')$ from the lookahead oracle,
the abstraction function replaces $e$ by a variable that is equal to $e$, or to $\perp$ if no such variable exists.
%When the mapper receives an abstract input from the learner, it computes a corresponding concrete
%input and forwards it to the lookahead oracle.
%During learning the abstract parameter value $\perp$ is always concretized to a fresh value.

\begin{definition}
We define $\A_F = \langle I' \cup O', X \cup Y,  R, r_0, \delta, \abstr \rangle$ where
%for all $r \in R$, $i \in I$, $o \in O$, $d, e \in \integers$, $w \in W \cup \{ \In \}$, and $\xi \in W \rightarrow \integers$,
\begin{itemize}
\item
$I' = I \times \integers$,
\item
$O' = \{ o(d ~ r) \mid o \in O \wedge d \in \integers \wedge r \in \integers^{\ast} \mbox{ injective} \}$,
\item
$X = \{ i(a) \mid i\in I \wedge a \in F(i) \cup \{ \perp \} \}$,
\item
$Y =  \{ o(a, \varrho) \mid o \in O \wedge a \in \V \cup \{ \perp \} \wedge \varrho \in  \V \nrightarrow \V \mbox{ injective with finite domain} \}$,
\item
$R = \{ r \in \integers^{\ast} \mid r \mbox{ injective} \}$,
\item
$r_0  = \epsilon$,
\item
$\delta(r, i(d)) = d ~ r$,
\item
$\delta(r, o (e ~ r')) = r'$,
\item
Let $r \in R$ and $i(d) \in I'$. 
Then
\begin{eqnarray*}
\lambda(r, i(d)) & = & \left\{ \begin{array}{ll}
                                i(\xi_r^{-1}(d)) & \mbox{if } d \in \ran(\xi_r) \mbox{ and } \xi_r^{-1}(d) \in F(i)\\
                                i(\perp) & \mbox{otherwise}
				\end{array} \right.
\end{eqnarray*}
Let $r = d ~ s \in R$ and $o(e ~ r') \in O'$.
Let $\iota_i$ be the valuation that is equal to $\xi_s$ if $d \in \ran (\xi_s)$ and equal to  $\xi_s \cup \{ (\In, d)\}$ otherwise.
Similarly, let $\iota_{io}$ be the valuation equal to $\iota_i$ if $e \in \ran (\iota_i)$ and equal to  $\iota_i \cup \{ (\Out, e)\}$ otherwise.
Then $\iota_{io}$ is injective and $\ran(\iota_{io}) = \ran(r) \cup \{ e \}$.
Suppose $\ran(r') \subseteq \ran(r) \cup \{ e \}$. 
Then $\varrho =  \iota_{io}^{-1} \circ \xi_{r'}$ is well-defined and injective, and
\begin{eqnarray*}
\lambda(r, o(e ~ r')) & = & \left\{ \begin{array}{ll}
                                (o(\iota_i^{-1}(e)), \varrho) & \mbox{if } e \in\ran(\iota_i)\\
                                (o(\perp), \varrho) & \mbox{otherwise}
				\end{array} \right.
\end{eqnarray*}
In the degenerate case $r = \epsilon$ or $\ran(r') \not\subseteq \ran(r) \cup \{ e \}$,
we define $\lambda(r, o(e ~ r'))= (o(\perp),\emptyset)$.
\end{itemize}
\end{definition}


\begin{example}
Consider an SUL that behaves as the FIFO-set model of Figure~\ref{fig:fifoSet}.
As a result of interaction with mapper $\A_{F_\emptyset}$, the learner may succeed to construct the abstract hypothesis shown in Figure \ref{fig:fifoSetHyp1}.
\begin{figure}[ht]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=4.3cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l_1$};
  \node[state] (3) [right of=2] {$l_2$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [bend left, text width=1.5cm] node {\push($\perp$)/\OK \\ $w_1$:=$\In$} (2)
        edge [loop above] node {\pop/\NOK} (1) %, text width=2.5cm
    (2) edge [bend left, text width=1.5cm] node {\push($\perp$)/\OK \\ $w_2$:=$\In$} (3)
        edge [bend left] node {\pop/\oout($w_1$)} (1)
    (3) edge [bend left, text width=2cm] node {\pop/\oout($w_1$)\\ $w_1$:=$w_2$} (2)
        edge [loop above] node {\push($\perp$)/\NOK} (3);
\end{tikzpicture}
\caption{First hypothesis for FIFO-set}
\label{fig:fifoSetHyp1}
\end{figure}
This first hypothesis is incorrect since it does not check if the same value is inserted twice. This is because
the Abstractor only generates fresh values during the learning phase.
Based on the analysis of a counterexample (to be discussed in the next section), Tomte will discover that it is relevant
whether or not the parameter of $\push$ is equal to the value of $w_1$. Consequently $F(\push)$ is set to $\{ w_1 \}$
and Tomte constructs a next hypothesis, for instance the one shown in Figure~\ref{fig:fifoSetHyp2}.
\begin{figure}[ht]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=4.3cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l_1$};
  \node[state] (3) [right of=2] {$l_2$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [bend left, text width=1.5cm] node {\push($\perp$)/\OK \\ $w_1$:=$\In$} (2)
        edge [loop above] node {\pop/\NOK} (1) %, text width=2.5cm
        edge [loop below] node {\push($w_1$)/$\perp$} (1)
    (2) edge [bend left, text width=1.5cm] node {\push($\perp$)/\OK \\ $w_2$:=$\In$} (3)
        edge [bend left] node {\pop/\oout($w_1$)} (1)
        edge [loop below] node {\push($w_1$)/\NOK} (2)
    (3) edge [bend left, text width=2cm] node {\pop/\oout($w_1$)\\ $w_1$:=$w_2$} (2)
        edge [loop below] node {\push($w_1$)/\NOK} (3)
        edge [loop above] node {\push($\perp$)/\NOK} (3);
\end{tikzpicture}
\caption{Second hypothesis for FIFO-set}
\label{fig:fifoSetHyp2}
\end{figure}
Note that, as the list of memorable values in the initial state is empty, there is no concrete action
$\push(d)$ that is abstracted to action $\push(w_1)$ in $l_0$. By the second rule from
Definition~\ref{def:Abstraction}, an abstract output $\perp$ is generated in this case.
\end{example}


\begin{theorem}
\label{theorem abstractor}
Let $\R$ be an input deterministic register automaton with input symbols $I$ and output symbols $O$ such
that each location has at most $n$ registers.
Let $S$ be the set of collision free neat traces of $\R$, and
let $T = \{ \Oracle_{\R}(\beta) \mid \beta \in S\}$, that is the set of traces from $S$
in which each output action is annotated with a list of memorable values of the corresponding target state.
Let $F$ be a function that assigns to each input symbol a subset of $\{ w_0 ,\ldots, w_{n-1} \}$.
Then $U = \ABS{T}{\A_F}$ is nonempty, prefix closed, input complete and $\equiv_U$ has finite index.
If moreover  $F(i) = \{ w_0 ,\ldots, w_{n-1} \}$, for all $i \in I$, then $U$ is behavior deterministic.
\end{theorem}

In order to show that an hypothesis is incorrect, we first need to concretize it. Using the theory of \cite{AJUV15} we
get a concretization operator for free, but this concretization operator produces unique-valued register
automata in which each output is annotated with the list of memorable values in the target state.
Since unique-valuedness leads to a loss of succinctness (and we no longer need the list of memorable values), 
we have implemented in Tomte an alternative procedure to concretize an abstract deterministic Mealy machine model to 
a right invariant register automaton:
\begin{enumerate}
\item
Omit all transitions with output $\perp$ (e.g.\  the $\push(w_1)$-loop in location $l_0$ of Figure~\ref{fig:fifoSetHyp2}).
\item
Whenever, for some location $l$ and input symbol $i$, there are transitions
$l \xrightarrow{i(\perp)/o(d), \varrho} l'$ and $l \xrightarrow{i(w_j)/o(d), \varrho} l'$, then omit the $i(w_j)$-transition
(e.g.\ the $\push(w_1)$-loop in location $l_2$ of Figure~\ref{fig:fifoSetHyp2};
apparently it does not matter whether or not the parameter of $\push$ is equal to the value of $w_1$).
\item
If, for some location $l$ and input symbol $i$, there are outgoing $i(w)$-transitions for each $w \in W$ then
add input guard $\bigwedge_{w \in W} \In \neq w$ to the $i(\perp)$ transition.
\item
If a transition has input label $i(w_j)$ then add input guard $\In = w_j$.
\item
If a transition has output label $o(\perp)$ then add output guard $\mathsf{true}$.
\item
If a transition has output label $o(w_j)$ then add output guard $\Out = w_j$.
\item
Replace input labels $i(d)$ by $i$, output labels $o(d)$ by $o$, and leave all the updates $\varrho$ unchanged.
\end{enumerate}
%TODO May need a theorem that in the limit case with F(i) maximal the result of our construction coincides with
%TODO the result of the concretization operator.

\begin{example}
If we apply the above procedure to the Mealy machine of Figure~\ref{fig:fifoSetHyp1}, then we obtain the register automaton
of Figure~\ref{fig:fifo} (modulo variable renaming), and if we apply it to the Mealy machine
of Figure~\ref{fig:fifoSetHyp2}, then we obtain the register automaton of Figure~\ref{fig:fifoSet} (again modulo
variable renaming).
\end{example}

In case function $F$ assigns the maximal number of abstract values to each input, the above concretization operator
will produce a unique-valued register automaton that is equivalent to the register automaton produced by the
concretization operator of \cite{AJUV15} (if we forget the lists of memorable values in output actions). 
In cases where $F$ is not maximal, our concretization operator will
typically produce register automata that are not unique-valued.
In the next section we will show how, when a flaw in the hypothesis is detected during the hypothesis verification phase, 
the resulting counterexample can be used for abstraction refinement.
