\section{Introduction}
\label{sec:intro}
Model checking and model learning are two core techniques in model-driven engineering.
In model checking \cite{CGP99} one explores the state space of a given state transition model,
whereas in model learning \cite{SteffenHM11,Hig10,Ang87,Vaa17} the goal is to obtain such a model through interaction
with a system by providing inputs and observing outputs.
Both techniques face a combinatorial blow up of the state-space, commonly known as the state explosion problem.
In order to find new techniques to combat this problem, it makes sense to follow a cyclic research methodology
in which tools are applied to challenging applications, the experience
gained during this work is used to generate new theory and algorithms,
which in turn are used to further improve the tools.
After consistent application of this methodology for 25 years 
model checking is now applied routinely to industrial problems \cite{25years}.
Work on the use of model learning in model-driven engineering started later \cite{PeledVY99}
and has not yet reached the same maturity level, but in recent years there has been spectacular progress.

We have seen, for instance, several convincing applications of model learning in the area of security and network protocols.
Cho et al. \cite{ChocSS10} successfully used model learning to infer models of communication protocols used by botnets.
Model learning was used for fingerprinting of EMV banking cards \cite{EMV13}.
It also revealed a security vulnerability in a smartcard reader for internet banking that was previously
discovered by manual analysis, and confirmed the absence of this flaw in an updated version of this device \cite{CPPR14}.
Fiterau et al. \cite{FJV14} used model learning to demonstrate that both Linux and Windows implementations violate the TCP protocol standard.
Using a similar approach, Fiterau et al. \cite{SSH17} showed that three implementations of the Secure Shell (SSH) protocol violate the standard.
In~\cite{RaMeSM2009}, model learning is used to infer properties of a network router, and for
testing the security of a web-application (the Mantis bug-tracker).
%The first application
%of learning in testing was presented in 2002 in~\cite{HaHuNS2002}: the authors use generated models for testing a telephony system.
Model learning has proven to be an extremely effective technique for spotting bugs, complementary to existing methods for software analysis.

A major theoretical challenge is to lift learning algorithms for finite state systems to richer classes of models involving data.
A breakthrough has been the definition of a Nerode congruence for a class of register automata \cite{CaHJMS2011,CasselHJMS15} and the
resulting generalization of learning algorithms to this class \cite{VMCAI2012,HISBJ12}.
%,CEGAR12,ThesisFides}.
Register automata \cite{KaminskiF94,CaHJMS2011} are a type of extended finite state machines in which one can test 
for equality of data parameters, but no operations on data are allowed. 
% This notion of a scalarset data type originates from model checking, where it has been used
%for symmetry reduction \cite{ID96} (hence register automata are also called scalarset Mealy machines in the literature).
Recently, the results on register automata have been generalized to even larger classes of models in which
guards may contain arithmetic constraints and inequalities \cite{CasselHJS16,Cassel2015}.
%
A different approach for extending learning algorithms to classes of models involving data has been proposed in \cite{AJUV15}.
Here the idea is to place an intermediate mapper component in between the implementation and the learner.
This mapper abstracts (in a history dependent manner) the large set of (parametrized) actions of the implementation into
a small set of abstract actions that can now be handled by automata learning algorithms for finite state systems.
In \cite{CEGAR12}, we described an algorithm that uses counterexample-guided abstraction refinement to automatically construct an appropriate mapper
for a subclass of register automata that may only store the first and the last occurrence of a parameter value.
%
Moerman et al. \cite{Moerman17} present a learning algorithm for \emph{nominal automata}, which are acceptors of languages
over infinite (structured) alphabets.
Nominal automata are a direct reformulation of the classical notion of finite automaton where one replaces finite
sets with orbit-finite sets and functions (or relations) with equivariant ones \cite{BKL14,Pitts13}. 
The algorithm of Moerman et al. \cite{Moerman17}
is almost a verbatim copy of the classical algorithm of Angluin \cite{Ang87}.
Deterministic nominal automata are equally expressive as register automata but, due to the fact that they are
unique-valued, exponentially less succinct (see \cite{CasselHJMS15}).

The approaches of \cite{CaHJMS2011,CasselHJMS15,VMCAI2012,HISBJ12,CasselHJS16,Cassel2015,CEGAR12,Moerman17} 
do not allow to learn models with of fresh output values.
Fresh outputs are technically challenging,\footnote{In register automata frameworks with accepting states, like \cite{CasselHJMS15}, fresh outputs can be modeled but membership queries cannot be implemented, whereas in transducer based frameworks
that generate outputs in response to inputs, like \cite{HISBJ12,CEGAR12}, the outcome of membership queries would become nondeterministic.}
but crucial in many real-world systems, e.g.\ servers that generate fresh identifiers, passwords or sequence numbers.
Bollig et al. \cite{Bollig2013} provide a learning algorithm for \emph{session automata},
a class of register automata that supports fresh data values. This algorithm is elegant, but the expressivity of session automata
is limited since no guards are allowed in transitions.

The main contributions of this article are
(a) an extension of the learning algorithm of \cite{CEGAR12} to the full class of register automate with fresh outputs,
(b) a description of the implementation of our new algorithm in the Tomte tool\footnote{\url{http://tomte.cs.ru.nl/}}, and
(c) an experimental evaluation of our implementation on a series of benchmarks, including a comparison with the \ralib{}\cite{Cassel2015} tool.
%As part of the LearnLib tool \cite{MertenSHM11,RSBM09}, a learning algorithm for register automata without fresh outputs has been implemented.
%In \cite{AartsHKV14}, we compared LearnLib with a previous version of Tomte (V0.3) 
%on a common set of benchmarks (without fresh outputs),
%a comparison that turned out favorably for Tomte.  
%Tomte, for instance, could learn a model of a FIFO-set buffer with capacity 30,
%whereas LearnLib could only learn FIFO-set buffers with capacity up to 7.
%
%This paper presents an experimental evaluation of the new \tomtenew{}. Due to evolution of the algorithm, \tomtenew{} significantly outperforms Tomte 0.3. Similar to the 0.4 version introduced in \cite{AFKV15}, Tomte can learn models for new benchmarks involving fresh outputs, only now it can also use and benefit from TTT \cite{Isberner2014}, a state of the art Mealy Machine learning algorithm which significantly reduces the number of learning queries. We compare \tomtenew{} to \ralib{}\cite{Cassel2015}, the successor of LearnLib, on a series of benchmarks.

%Tomte implements the approach to inferring Register Mealy Machines presented in \cite{CEGAR12,ThesisFides}.
Figure~\ref{fig:learningComponents} presents the overall architecture of our learning approach.
At the right we see the \emph{teacher} or \emph{system under learning (SUL)},
%\todo{change SUL -$>$ SUL throughout paper}
an implementation whose behavior can be described by an (unknown) input enabled and input deterministic register automaton.
At the left we see the \emph{learner}, which is a tool for learning finite deterministic Mealy machines.
In our current implementation we use LearnLib \cite{MertenSHM11,RSBM09}, but there are also other libraries like libalf \cite{BolligEtAl10} that
implement active learning algorithms.
In between the learner and the SUL we place three auxiliary components:
the \emph{determinizer}, the \emph{lookahead oracle}, and the \emph{abstractor}.
First the determinizer eliminates the nondeterminism of the SUL that is induced by fresh outputs.
Then the lookahead oracle annotates events with information about the data values that need to be remembered 
because they play a role in the future behavior of the SUL.
Finally, the abstractor maps the large set of concrete values of the SUL to a small set of symbolic values that can be handled by the learner.

\begin{figure}[h!]
\centering
\begin{tikzpicture}
  [node distance=.3cm, start chain=going right]
     \node[punktchain] (learner) {Learner};
     \node[punktchain] (mapper) {Abstractor};
     \node[punktchain] (traceWarehouse) {Lookahead \\ Oracle};
     \node[punktchain] (determinizer) {Determinizer};
     \node[punktchain] (teacher) {Teacher \\ (SUL)};

     % compute a point in the middle between E and NE or W and NW, then draw a line between these two
     \path (learner.east) -- (learner.north east) coordinate[pos=0.5] (learner2);
     \path (mapper.west) -- (mapper.north west) coordinate[pos=0.5] (mapper1);
     \draw[thick,-latex] (learner2) -- (mapper1);
     %\node [above] at (1.4,0.2) {1};

    \path (mapper.east) -- (mapper.north east) coordinate[pos=0.5] (mapper2);
    \path (traceWarehouse.west) -- (traceWarehouse.north west) coordinate[pos=0.5] (traceWarehouse1);
    \draw[thick,-latex] (mapper2) -- (traceWarehouse1);
    %\node [above] at (4.2,0.2) {2};

    \path (traceWarehouse.east) -- (traceWarehouse.north east) coordinate[pos=0.5] (traceWarehouse2);
    \path (determinizer.west) -- (determinizer.north west) coordinate[pos=0.5] (determinizer1);
    \draw[thick,-latex] (traceWarehouse2) -- (determinizer1);

    \path (determinizer.east) -- (determinizer.north east) coordinate[pos=0.5] (determinizer2);
    \path (teacher.west) -- (teacher.north west) coordinate[pos=0.5] (teacher1);
    \draw[thick,-latex] (determinizer2) -- (teacher1);
    %\node [above] at (7.0,0.2) {3};

    \path (learner.east) -- (learner.south east) coordinate[pos=0.5] (learner2);
    \path (mapper.west) -- (mapper.south west) coordinate[pos=0.5] (mapper1);
    \draw[thick,latex-] (learner2) -- (mapper1);
    %\node [below] at (1.4,-0.2) {6};

    \path (mapper.east) -- (mapper.south east) coordinate[pos=0.5] (mapper2);
    \path (traceWarehouse.west) -- (traceWarehouse.south west) coordinate[pos=0.5] (traceWarehouse1);
    \draw[thick,latex-] (mapper2) -- (traceWarehouse1);
    %\node [below] at (4.2,-0.2) {5};

    \path (traceWarehouse.east) -- (traceWarehouse.south east) coordinate[pos=0.5] (traceWarehouse2);
    \path (determinizer.west) -- (determinizer.south west) coordinate[pos=0.5] (determinizer1);
    \draw[thick,latex-] (traceWarehouse2) -- (determinizer1);

    \path (determinizer.east) -- (determinizer.south east) coordinate[pos=0.5] (determinizer2);
    \path (teacher.west) -- (teacher.south west) coordinate[pos=0.5] (teacher1);
    \draw[thick,latex-] (determinizer2) -- (teacher1);
    %\node [below] at (7.0,-0.2) {4};
\end{tikzpicture}
\caption{Architecture of Tomte}
\label{fig:learningComponents}
\end{figure}

The idea to use an abstractor for learning register automata originates from \cite{CEGAR12} (based on work of \cite{AJUV15}).
Using abstractors one can only learn restricted types of deterministic register automata.
Therefore, \cite{ThesisFides,AartsHKV14} introduced the concept of a lookahead oracle, which makes it possible to learn any deterministic register automaton.
In this paper, we extend the algorithm of \cite{ThesisFides,AartsHKV14} with the notion of a determinizer, allowing us to also learn register automata
with fresh outputs.
