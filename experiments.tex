\section{Evaluation and Comparison}
\label{sec:evaluation}
In this section,
we compare \tomtenew{} to other learning tools on a series of benchmarks including
the Session Initiation Protocol (SIP), 
the Alternating Bit Protocol, 
the Biometric Passport, 
FIFO-Sets, and 
a multi-Login system. 
Apart from the last one, all 
these benchmarks have already been used in~\cite{AartsHKV14} for the comparision of \tomteold{}, a previous version of Tomte, and 
\learnlib{}. 
In~\cite{AFKV15}, we compared Tomte 0.4 with \learnlib{} and \tomteold{}, concluding that Tomte 0.4 performed best in all but two benchmarks. 
%
Since then, \ralib{}\cite{Cassel2015} has been released, a learner  building on  \learnlib{}, adding several optimizations as well as 
enabling support for theories other than equality. This made \ralib{} a strong competitor, reporting better numbers for a number of 
benchmarks. Tomte itself was also improved and can now work with TTT \cite{Isberner2014}, a new  and fast algorithm for learning Mealy
Machines. We focus our evaluation efforts on the more novel \tomtenew{} and \ralib{}. Readers are referred to \cite{AartsHKV14} and \cite{AFKV15} for benchmarking of the 0.3 and 0.4 versions of Tomte and \learnlib{}. \tomtenew{} generally replicates the numbers obtained by version 0.4 in those benchmarks.

Each experiment consists of learning a simulation of a model implementing a benchmark system or, 
as in the case of the multi-login system, learning of an actual implementation. 
Whenever possible we verified the learned model by performing an equivalence check against the simulated model. 
For the multi-login system  we ran a thorough suite of tests.
For the FIFO-Set models, we  checked the models manually by analyzing the number of states and guards in the learned model.

\InputTable{experimentsjournal}

\tomtenew{} can now be configured to work with different Mealy Machine learners. Traditionally, we have used the Observation Pack algorithm \cite{ThesisFalk}, which is enabled in all versions of Tomte. Recently, we have adapted \tomtenew{} to support the new TTT algorithm \cite{Isberner2014}. Similarly, \ralib{} adopts a series of optimizations. We enable all these optimizations apart from the one exploiting parameter typing (unlike in ~\cite{Cassel2015}), since all benchmarks used are not typed.

Table~\ref{tab:experiments} provides benchmarks for \tomtenew{} using each of TTT and Observation Pack, and \ralib{} with the optimizations mentioned. 
Results for each model are obtained by running each learner configuration 10 times with 10 different seeds. Over 
these runs we collect the average and standard deviation for number of reset queries and inputs
applied during learning (denoted \textbf{learn res} and \textbf{learn inp}), counterexample analysis 
(denoted \textbf{ana res} and \textbf{ana inp}) and testing (denoted \textbf{test res} and \textbf{test inp}). The numbers for testing
do not include queries run on the final hypothesis. As \ralib{} does not distinguish counterexample analysis from learning and testing, 
we exclude statistics for this phase. A final statistic is success (\textbf{succ}), denoting for each model the number of 
successful experiments, that is, experiments which ended with the correct model learned. Since \tomtenew{} is always successful, we exclude this statistic from its columns.
 
For consistency, we use the same equivalence oracle across all learners, namely, a random walk oracle configured with a maximum 
test query length of 100 and an average length of 10, with a maximum of 1000000 tests per equivalence query. The probability of selecting a fresh value is set to 0.1 .
We opted for this algorithm, since it was the only algorithm supported \ralib{}. In contrast, \tomtenew{} can also use more advanced testing algorithms.  
When learning FIFO-Set 30 we  increase the average query length to 100, otherwise testing would most likely fail to find all counterexamples. Similarly, for FIFO-Set 14
we increase it to 50.

We omit running times, as we consider  the number of queries to be a superior metric of measuring efficiency, but the reader may find them at \url{http://automatalearning.cs.ru.nl/}. All models apart from the multi-logins and large FIFO-Set models are learned in less than one minute. 
We limit learning time to 20 minutes. 

Results show that TTT significantly brings down the number of learning queries needed by \tomtenew{}, at the cost of more test and counterexample analysis queries. 
This cost is offset for all but the first model benchmarked. The extent of improvement when we consider the sum of all inputs varies from roughly a 23 \% reduction 
for the SIP model to a factor of 8 reduction for the Palindrome Checker. We also notice that the gap widens with the growing complexity of the models. Furthermore,
improvement would likely have been greater had a smarter testing algorithm been used. 

\ralib{} beats \tomtenew{} on several models, particularly SIP and FIFO-Set 7. Unfortunately, its performance is highly erratic, as shown by the high standard deviation. 
Moreover, \ralib{} is only partially successful at learning some models, while failing completely to learn others.  Ultimately, RALib shows promising numbers for some experiments,  while for others
it seems to suffer a blow up in its algorithm. For the larger models, like the FIFO-Set 30, RALib fails completely.


The multi-login system benchmark can only be properly handled by \tomtenew{}. 
The benchmark generalizes the example of Figure~\ref{fig:login} to multiple users, while adding an additional 
user ID parameter when logging in and registering. 
A configurable number of users may register, enabling simultaneous login sessions for different registered users.
\tomtenew{} was able to successfully learn instantiations of multi-login systems for 1, 2 and 3 users. \ralib{} struggled
to learn configurations with 1 user, while completely failing for those with more users.

That said, \tomtenew{}'s learning algorithm also does not perform nor scale well for higher numbers of users. This can be ascribed to the high number of global abstractions. Such a number is owing to not only the large number of registers, but also to the varying order in which memorable values are found per state.  

A memorable value, be it login id or password, can take one index in one state, but another index in a different state. As we use global abstractions, the memorable value would require two distinct abstractions, even though only one is useful in each state. This leads
to a large number of abstractions required to cover all indexes memorable values can take.


%Because
%we use global abstractions, this leads 
%This can lead to different abstractions used in different states to refer to the same memorable value. 

%A user can register, providing
%a login ID. Unless the maximum number of registered users, or $regMax$, is exceeded, the $Register$ input
%will yield an $OK$ output containing a password that can later be used by the user to log into the system.
%If $regMax$ is reached, $Register$ returns $NOK$, meaning the user was not registered.  Once successfully 
%registered the user can login ($Login$). Logged in, the user can change their password ($ChangePassword$) or 
%log out ($Logout$). There is no limit on the number of users that can be logged in at any point in time other
%than $regMax$, which sets an upper bound to the number of users. \tomtenew{} infers models for instantiations of 
%Multi-Login Systems with 1, 2 and 3 users. It fails, however, to learn instances where $regMax > 3$ because of poor scalability.

%We generated and learned instances of Multi-Login Systems for increasing values of $regMax$. In [figure], we present minimal RA's
%for $regMax$ 1 and 2 respectively. Note that the number of locations of such an automaton can be calculated as 
%$\binom{3 + regMax - 1}{regMax}$ where 3 suggests the number of states a user can be found in, that is not registered, registered 
%and logged in. Tomte 0.4 infers models for instantiations of Multi-Login Systems with 1, 2 and 3 users. It fails, however,
%to learn instances where $regMax > 3$ because of poor scalability. 

%* scalability better defined
%The issue of scalability can be ascribed to the large number of abstractions found for every concrete action. 
%This in turn is caused by the large number of memorable values in the system. Many such 
%abstractions don't make sense for certain states of the system, for example, applying a $Login$ abstract input
%with a login ID equal to a memorized password value. A solution to this problem is implementing state based abstractions,
%whereby only certain abstract inputs from the alphabet are enabled in each state. But such a technique
%is complex and has not been implemented in \tomtenew{}.
%which in turn leads to a large number of parameter abstractions. 


