\section{Register Automata}
\label{sec:aal}

In this section, we define register automata and their operational semantics in terms of Mealy machines.
In addition, we discuss technical concepts that provide insight into the behavior of register automata, concepts which
play a key role in the technical development of this paper: right invariance and symmetry.
For reasons of exposition, the notion of \emph{register automaton} that we define here is a simplified version of what
we have implemented in our tool: Tomte also supports constants and actions with multiple parameters.
Our definition is similar to that of a \emph{scalarset Mealy machine} from \cite{CEGAR12} and of a \emph{register Mealy machine}
from \cite{HISBJ12}, except that the value of the ouput parameter is not necessarily determined
by the values of the registers and the input parameter.

\subsection{Definition}
We postulate a countably infinite set $\V$ of \emph{variables}, which contains two special variables $\In$ and $\Out$.
An \emph{atomic formula} is a boolean expression of the form $\mathsf{true}$, $\mathsf{false}$, $x = y$ or $x \neq y$, 
with $x, y \in \V$.
A \emph{formula} $\varphi$ is a conjunction of atomic formulas.
Let $X \subseteq \V$ be a set of variables.  
We write $\G(X)$ for the set of formulas with variables taken from $X$.
A \emph{valuation} for $X$ is a function $\xi : X \rightarrow \integers$.
We write $\Val(X)$ for the set of valuations for $X$.
If $\varphi$ is a formula with variables from $X$ and $\xi$ is a valuation for $X$, then we write
$\xi \models \varphi$ to denote that $\xi$ satisfies $\varphi$.
%Here $\xi \models x = y$ iff $\xi(x) = \xi(y)$.
We use symbol $\equiv$ to denote syntactic equality of formulas.
We represent (partial) functions as sets of pairs, and write $X \nrightarrow Y$ for the set of partial functions
from $X$ to $Y$.

\begin{definition}[Register automaton]
A \emph{register automaton (RA)} is a tuple $\R = \langle I, O, L, l_{0}, V, \Gamma\rangle$ with
\begin{itemize}
%NOTE: I refer to the inputs and outputs of a RA as symbols, and refer to the inputs and outputs of a Mealy machine
%as actions. Symbols for me refers to something from a finite domain.
\item $I$ and $O$ finite, disjoint sets of \emph{input} and \emph{output symbols}, respectively,
%NOTE: I am referring here to locations, in contrast with the states of a Mealy machine.
\item $L$ a finite set of \emph{locations} and $l_0 \in L$ the \emph{initial location},
\item $V$ is a function that assigns to each location $l$ a finite set 
$V(l) \subseteq \V \setminus \{ \In, \Out \}$ of \emph{registers},
with $V(l_0 ) = \emptyset$.
\item $\Gamma \subseteq L \times I \times \G(\V) \times (\V \nrightarrow \V) \times O \times L$ a finite set of \emph{transitions}.
For each transition $\langle l, i, g, \varrho, o, l' \rangle \in \Gamma$, we refer to $l$ as the \emph{source},
$i$ as the input symbol, $g$ as the \emph{guard}, $\varrho$ as the \emph{update}, $o$ as the \emph{output symbol}, and $l'$ as the \emph{target}.
We require that $g \in \G(V(l) \cup \{ \In, \Out \})$ and 
%$\Out$ does not occur negatively in $g$, that is, not in a subformula of the form $x \neq y$. We also require that 
$\varrho : V(l') \rightarrow V(l) \cup \{ \In, \Out \}$.
We write $l \xrightarrow{i, g, \varrho, o} l'$ if $\langle l, i, g, \varrho, o, l' \rangle \in \Gamma$.
\end{itemize}
%Require that for each l and i the guards of the outgoing transitions are pairwise disjoint?
%We require register automata to be \emph{input enabled} in the sense that for each location $l$ and for each input $i$
%the disjunction of the guards of the transitions with source $l$ and input $i$ is valid.
%We say $\SMM$ is \emph{deterministic} if, for all distinct transitions
%$\tau_1 = \langle l_1, e^I_1, g_1, \varrho_1, e^0_1, l'_1 \rangle$ and
%$\tau_2 = \langle l_2, e^I_2, g_2, \varrho_2, e^0_2, l'_2 \rangle$ in $\Gamma$,
%$l_1 = l_2$ and $e^I_1 = e^I_2$ implies $g_1 \wedge g_2 \equiv {\sf false}$.
\end{definition}

%The requirement that $\Out$ does not occur negatively in guards implies that there are two types of transitions: 
%transitions in which there are no constraints on the value of $\Out$, and 
%transitions in which the value of $\Out$ is equal to the value of some of the other variables in $V \cup \{ \In \}$.

\begin{example}
As a first running example of a register automaton we use a FIFO-set with capacity two, similar to the one presented in \cite{HISBJ12}.
A FIFO-set is a queue in which only different values can be stored, see Figure \ref{fig:fifoSet}.
\begin{figure}[htb]
\centering
\InputTikz{fifoSet}
\caption{FIFO-set with a capacity of 2 modeled as a register automaton}
\label{fig:fifoSet}
\end{figure}
In this automaton, $I = \{ \push, \pop \}$, $O = \{ \OK, \NOK, \oout \}$, $L = \{ l_0, l_1, l_2 \}$, $V(l_0 ) = \emptyset$,
$V(l_1) = \{ v \}$, and $V(l_2) = \{ v,w \}$.
Input $\push$ tries to add the value of parameter $\In$ to the queue,
and input $\pop$ tries to retrieve a value from the queue.
The output in response to a $\push$ is $\OK$ if the input value can be added successfully,
or $\NOK$ if the input value is already in the queue or if the queue is full.
The output in response to a $\pop$ is $\oout(\Out)$, with as parameter the oldest value
from the queue, or $\NOK$ if the queue is empty.
Each input has parameter $\In$ and each output has parameter $\Out$. However,
we omit parameters that do not matter and for instance write $\pop$ instead of $\pop(\In)$ since
parameter $\In$ does not occur in the guard and is not touched by the update.
Usually, we also do not list the sets of variables of locations explicitly, as they can be inferred from the context.
\end{example}

\subsection{Semantics}
The operational semantics of register automata is defined in terms of (infinite state) Mealy machines.

\begin{definition}[Mealy machine]
%NOTE: I removed restriction that I and O are nonempty!
A \emph{Mealy machine} is defined to be a tuple $\M = \langle I, O, Q, q^0, \rightarrow \rangle$, where
$I$ and $O$ are disjoint sets of \emph{input} and \emph{output actions}, respectively, 
$Q$ is a set of \emph{states}, $q^0 \in Q$ is the \emph{initial state}, and
${\rightarrow} \subseteq Q \times I \times O \times Q$ is the \emph{transition relation}.
We write $q \xrightarrow{i/o} q'$ if $(q, i, o, q') \in {\rightarrow}$, and
$q \xrightarrow{i/o}$ if there exists a state $q'$ such that $q \xrightarrow{i/o} q'$.
A Mealy machine is \emph{input enabled} if, for each state $q$ and input $i$,
there exists an output $o$ such that $q \xrightarrow{i/o}$.
We say that a Mealy machine is \emph{finite} if the sets $Q$, $I$ and $O$ are finite.
\end{definition}

A \emph{partial run} of $\M$ is a finite sequence
%\begin{eqnarray*}
$\alpha  =  q_0 ~ i_0 ~ o_0 ~ q_1 ~ i_1 ~ o_1 ~ q_2 \cdots i_{n-1} ~ o_{n-1} ~ q_n$,
%\end{eqnarray*}
beginning and ending with a state, such that for all $j < n$, $q_j \xrightarrow{i_j/o_j} q_{j+1}$. 
A \emph{run} of $\M$ is a partial run that starts with $q^0$.
The \emph{trace} of $\alpha$, denoted $\trace(\alpha)$,
is the finite sequence $\beta = i_0 ~ o_0 ~ i_1 ~ o_1 \cdots i_{n-1} ~ o_{n-1}$
that is obtained by erasing all the states from $\alpha$.
We say that $\beta$ is a \emph{trace} of state $q \in Q$ iff $\beta$ is the trace of some partial run that starts in $q$, and
we say that $\beta$ is a \emph{trace} of $\M$ iff $\beta$ is a trace of $q^0$.
We call two states $q, q' \in Q$ \emph{equivalent}, notation $q \approx q'$, iff they have the same traces.
Let $\M_1$ and $\M_2$ be Mealy machines with the same sets of input actions.
We say that $\M_1$ and $\M_2$ are \emph{equivalent}, notation $\M_1 \approx \M_2$, if they have the same traces.
We say that $\M_1$ \emph{implements} $\M_2$, notation $\M_1 \leq \M_2$, if all traces of $\M_1$ are also traces of $\M_2$.

%We write $\epsilon$ to denote the empty sequence.
%We write $|\beta|$ to denote the length of a sequence $\beta$.
%We call $\M$ \emph{behavior deterministic} if its set of traces is so.

The operational semantics of a register automaton is a Mealy machine in which the states are pairs of
a location $l$ and a valuation $\xi$ of the state variables. A transition may fire
for given input and output values if its guard evaluates to true.
In this case, a new valuation of the state variables is computed using the update part of the transition.

\begin{definition}[Semantics register automata]
\label{semantics SMM}
Let $\R=\langle I, O, L, l_0, V, \Gamma \rangle$ be a RA.
The operational semantics of $\R$, denoted $\sem{\R}$, is the Mealy machine
$\langle I \times \integers, O \times \integers, Q, q^0, \rightarrow\rangle$, where
$Q = \{ (l,\xi)  \mid l \in L \wedge \xi \in \Val(V(l))) \}$, $q^0 = (l_0, \emptyset)$, 
and relation $\rightarrow$ is defined inductively by the rule
\begin{equation}
\label{semantics rule}
\frac{\begin{array}{c}
l \xrightarrow{i, g, \varrho, o} l'\\
\iota = \xi \cup \{ (\In, d), (\Out, e) \}  \hspace{5mm} \iota\models g \hspace{5mm} \xi' = \iota\circ\varrho
\end{array}}
{
(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')
}
\end{equation}
If transition $(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')$ can be inferred using rule (\ref{semantics rule}) then we
say that it is \emph{supported} by transition $l \xrightarrow{i, g, \varrho, o} l'$ of $\R$,
and that transition $l \xrightarrow{i, g, \varrho, o} l'$ \emph{fires}.

We call $\R$ input enabled if its operational semantics $\sem{\R}$ is input enabled.
A run or trace of $\R$ is just a run or trace of $\sem{\R}$, respectively.
Two register automata $\R_1$ and $\R_2$ are \emph{equivalent} if $\sem{\R_1}$ and $\sem{\R_2}$ are equivalent.
We call $\R$ \emph{input deterministic} if for each reachable state $(l, \xi)$ and input action $i(d)$ 
%and output action $o(e)$,
at most one transition may fire.
An input deterministic register automaton $\R$ has the property that for any trace $\beta$ of $\R$ there exists a unique run $\alpha$
such that $\trace(\alpha) = \beta$.
\end{definition}

\begin{example}
The register automaton of Figure~\ref{fig:fifoSet} is input deterministic.
The following sequence constitutes a run of this automaton:
\[
(l_0, \emptyset) \xrightarrow{\pop/\NOK} (l_0, \emptyset) \xrightarrow{\push(22)/\OK} (l_1, \{ (v,22) \}) 
\xrightarrow{\push(7)/\OK} (l_1, \{ (v,22), (w,7) \})
\]
By erasing the states from this sequence we obtain the trace
\[
\pop ~ \NOK ~ \push(22) ~ \OK ~ \push(7) ~ \OK
\]
Note that we again omit the parameters of actions $\pop$, $\NOK$ and $\OK$.
\end{example}

The main contribution of this paper is an algorithm for learning input enabled and input deterministic register automata.
Our algorithm solves this problem by reducing it to the problem of learning finite \emph{deterministic} Mealy machines,
for which efficient algorithms exist. We recall the definition of a deterministic Mealy machine.
We call a register automaton deterministic if its semantics is a deterministic Mealy machine.

\begin{definition}[Deterministic Mealy machine]
A Mealy machine $\M = \langle I, O, Q, q^0, \rightarrow \rangle$ 
is \emph{deterministic} if for each state $q$ and input action $i$ there is exactly one
output action $o$ and exactly one state $q'$ such that $q \xrightarrow{i/o} q'$.
A deterministic Mealy machine $\M$ can equivalently be represented as a structure
$\langle I, O, Q, q^0, \delta, \lambda \rangle$, where $\delta : Q \times I \rightarrow Q$ and
$\lambda : Q \times I \rightarrow O$ are defined by:
$q \xrightarrow{i/o} q' ~ \Leftrightarrow ~ \delta(q,i) = q' \wedge \lambda(q,i) = o$.
Update function $\delta$ is extended to a function from $Q \times I^{\ast} \rightarrow Q$ by
the following classical recurrence relations:
\begin{eqnarray*}
%\label{eq:deltaEps}
\delta(q, \epsilon) & = & q,\\
%\label{eq:deltaAct}
\delta(q, i \; u) & = & \delta(\delta(q, i), u).
\end{eqnarray*}
Similarly, output function $\lambda$ is extended to a function from $Q \times I^{\ast} \rightarrow O^{\ast}$ by
\begin{eqnarray*}
%\label{eq:lambdaEps}
\lambda(q, \epsilon) & = & \epsilon,\\
%\label{eq:lambdaAct}
\lambda(q, i \; u) & = & \lambda(q,i) \; \lambda(\delta(q,i), u).
\end{eqnarray*}
\end{definition}

\begin{example}
The register automaton of Figure~\ref{fig:fifoSet} is not deterministic.
Recall that in every transition of a register automaton the input symbol carries a parameter $\In$ and the output symbol
carries a parameter $\Out$, but that we omit these parameters in diagrams when they do not occur in the guard and are not touched by the update.
As there are no constraints on the value of $\Out$ for transitions with output symbol $\OK$,
an input $\push(1)$ may induce both an $\OK(1)$ and an $\OK(2)$ output (in fact, parameter $\Out$ can take any value).
We can easily make the automaton of Figure~\ref{fig:fifoSet} deterministic, for instance by strengthening the guards with $\Out = \In$ for transitions where the output value does not matter.
\end{example}

\begin{example}
Our second running example is a register automaton, displayed in Figure \ref{fig:login}, that describes a simple login procedure.
\begin{figure}[hbt]
\centering
\InputTikz{login}
\caption{A simple login procedure modeled as a register automaton}
\label{fig:login}
\end{figure}
If a user performs a $\register$-input then the automaton produces output symbol $\OK$ together with a password.
The user may then proceed by performing a $\login$-input together with the password that she has just received.
After login the user may either change the password or logout.
We can easily make the automaton input enabled by adding self loops $i/\NOK$ in each location, for each input symbol $i$ that is not enabled.
It is not possible to model the login procedure as a deterministic register automaton: the very essence of
the protocol is that the system nondeterministically picks a password and gives it to the user.
\end{example}


\subsection{Symmetry}
A key characteristic of register automata is that they exhibit strong symmetries. Because no operations on data values
are allowed and we can only test for equality, bijective renaming of data values 
preserves behavior. The symmetries can be formally expressed through the notion of an automorphism.
In the remainder of this section, we present the definition of an automorphism and explore some basic properties that will
play a key role later on in this article.
Slight variations of these properties have been proven elsewhere, see for instance \cite{Hei12}.
The symmetry of register automata under data automorphisms plays a central role in \cite{BKL14}, 
and in fact serves as a definition of the equivalent notion of a nominal automaton. 

\begin{definition}
An \emph{automorphism} is a bijection $h : \integers \rightarrow \integers$.
\end{definition}

Let $X$ be a set of variables. Then we lift an automorphism $h$ to valuations $\xi$ for $X$ by pointwise extension, that is,
$h(\xi) = h \circ \xi$.
Since formulas in $\G(X)$ only assert that variables from $X$ are equal or not,
satisfaction of these formulas is not affected when we apply an automorphism to a valuation.

\begin{lemma}
\label{automorphism preserves satisfaction}
Let $h$ be an automorphism, $X$ be a set of variables, $\xi \in \Val(X)$ and $\varphi \in \G(X)$.
Then $\xi \models \varphi$ iff $h(\xi) \models \varphi$.
\end{lemma}
\iflong
\begin{proof}
By structural induction on $\varphi$.
\qed
\end{proof}
\fi

We also lift automorphisms to the states, actions and transitions of 
a register automaton $\R$ by pointwise extension. The transition relation of $\R$ is preserved by automorphisms.

\begin{lemma}
\label{automorphism preserves transitions}
Let $h$ be an automorphism and let $\R$ be a register automaton.
Then $(l,\xi)\xrightarrow{i(d) / o(e)}(l',\xi')$ is a transition of $\R$ iff
$(l,h(\xi))\xrightarrow{i(h(d)) / o(h(e))}(l',h(\xi'))$ is a transition of $\R$.
\end{lemma}
\iflong
\begin{proof}
Use Lemma~\ref{automorphism preserves satisfaction}.
\qed
\end{proof}
\fi

Next, we lift automorphisms to runs by pointwise extension.

\begin{lemma}
\label{automorphism preserves runs}
Let $h$ be an automorphism and let $\R$ be a register automaton.
Then $\alpha$ is a (partial) run of $\R$ iff $h(\alpha)$ is a (partial) run of $\R$.
\end{lemma}
\iflong
\begin{proof}
Use Lemma~\ref{automorphism preserves transitions} and the fact that $h$ trivially
preserves the initial state.
\qed
\end{proof}
\fi

Finally, we lift automorphisms to traces by pointwise extension.

\begin{lemma}
\label{trace and automorphism commute}
Let $h$ be an automorphism, let $\R$ be a register automaton and let $\alpha$ be a partial run of $\R$.
Then $\trace(h(\alpha)) = h (\trace(\alpha))$.
\end{lemma}
\iflong
\begin{proof}
Use Lemma~\ref{automorphism preserves runs}.
\qed
\end{proof}
\fi

\begin{corollary}
Let $h$ be an automorphism and let $\R$ be a register automaton.
Then $\beta$ is a trace of $\R$ iff $h(\beta)$ is a trace of $\R$.
\end{corollary}

We call two states, actions, transitions, runs or traces \emph{equivalent} if there exists an automorphism that maps one to the other.

\subsection{Constants and multiple parameters}
Tomte also supports constants and actions with multiple parameters. These features are convenient for modelling
applications, but do not add any expressivity to the basic model of register automata.

Suppose $\R$ is a register automaton in which we would like to refer to  distinct constants $c_1$ and $c_2$.
Then we may extend $\R$ with the sequence of two transitions illustrated in Figure~\ref{fig:constants}, starting
from location $l'_0$ which is the initial location of the extended automaton.
\begin{figure}[bht]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l'_0$};
  \node[state] (2) [right of=1] {$l'_1$};
  \node[state] (3) [right of=2] {$l_0$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=2cm] node {\initialize(\In)/\OK \\ $c_1$:=$\In$} (2)
    (2) edge [text width=1.5cm] node {$\In \neq c_1$ \\ \initialize(\In)/\OK\\ $c_2$:=$\In$} (3);
\end{tikzpicture}
\caption{Encoding of constants}
\label{fig:constants}
\end{figure}
The first transition initializes $c_1$, which becomes a variable in our encoding, and similarly the second transition
initializes $c_2$.
After performing the initializations we enter the initial state $l_0$ of $\R$.
Constants $c_1$ and $c_2$ are added as variables to all the locations of $\R$, and they may be tested in transitions.
The encoding introduces an auxiliary input symbol $\initialize$ and output symbol $\OK$. If desired, the register
automaton can be made input enabled by adding a trivial $\initialize$-loop to each location of $\R$, and an
$\initialize$-loops with guard $\In = c_1$ to $l'_1$.
Note that in an actual run of the automaton, $c_1$ and $c_2$ may be assigned arbitrary (distinct) values,
different from the specific values for these constants that we had in mind originally.
However, because of the symmetries of register automata this does not matter, and we may always rename
constants to their intended values via an appropriate automorphism.

Tomte also supports multiple parameters for input and output actions, like in the simple login model
shown in Figure~\ref{fig:Login2}. This model describes a system in which a user can register by providing a user id and a password,
and then login using the credentials that were used for registering.
\begin{figure}[htb]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=4cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l_1$};
  \node[state] (3) [right of=2] {$l_2$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=2cm] node {\register($\In_1, \In_2$)/\OK \\ $\usr$:=$\In_1$\\$\pwd$:=$\In_2$} (2)
    (2) edge [text width=1.5cm] node {$\In_1 = \usr$\\ $\In_2 = \pwd$ \\ \login($\In_1, \In_2$)/\OK} (3)
        edge [loop above, text width=2.5cm] node {$\In_1 \neq \usr \vee \In_2 \neq \pwd$ \\ \login($\In_1, \In_2$)/\NOK} (2)
        edge [loop below, text width=2.5cm] node {\register/\NOK} (2)
    (3) edge [loop above, text width=2.5cm] node {\login/\NOK} (3)
        edge [loop below, text width=2.5cm] node {\register/\NOK} (3);
\end{tikzpicture}
\caption{A simple login system with inputs that carry two parameters}
\label{fig:Login2}
\end{figure}
What we can do here is to split a transition with multiple input parameters into a squence of transitions with a
single parameter. The transition from $l_0$ to $l_1$, for instance, can be translated to the pattern shown
in Figure~\ref{fig:multipleparameters}.
\begin{figure}[bht]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,main node/.style={circle,draw,font=\sffamily\large\bfseries}]
  \node[initial, state] (1) {$l_0$};
  \node[state] (2) [right of=1] {$l'_0$};
  \node[state] (3) [right of=2] {$l_1$};

  \path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [text width=2cm] node {\register(\In)/\OK \\ $\usr$:=$\In$} (2)
    (2) edge [text width=1.5cm] node {\register(\In)/\OK\\ $\pwd$:=$\In$} (3);
\end{tikzpicture}
\caption{Encoding of multiple parameters}
\label{fig:multipleparameters}
\end{figure}

The implementation in Tomte involves several optimizations and does not use the above encodings.
Nevertheless, these encodings show how constants and multiple parameters can be handled conceptually.
